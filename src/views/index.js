/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { Container, CardDeck, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Breadcrumb, BreadcrumbItem } from "reactstrap";
// plugin que permite crear graficos en react
import Header from "components/header/Header.js";

class index extends React.Component {

    render() {
        return (
            <>
                <Header />
                <Container className="cajon-inicio-mt-1 mb-3" fluid>
                    <div>
                        <Breadcrumb className="portapapeles">
                            <span className="material-icons icon-title">public</span>
                            <BreadcrumbItem className="title-container">Inicio rapido</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <CardDeck>
                        <Card className="card-style">
                            <CardImg top width="100%" className="img-card-index" src={require("assets/img/img29.jpg")} alt="Card image cap" />
                            <CardBody>
                                <CardTitle className="title-card"><span className="material-icons icono-card">directions_bus</span>Transporte</CardTitle>
                                <CardSubtitle className="subtitle-card">Recorre Antioquia</CardSubtitle>
                                <CardText className="text-card">Registrar proveedores de transporte </CardText>
                                <div className="box-btn-see-detail">
                                    <button type="button" className="button-card-footer" >INGRESAR<span class="material-icons icon-button">arrow_forward</span></button>
                                </div>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg top width="100%" className="img-card-index" src={require("assets/img/img24.jpg")} alt="Card image cap" />
                            <CardBody>
                                <CardTitle className="title-card"> <span className="material-icons icono-card">local_see</span> Sitios turisticos</CardTitle>
                                <CardSubtitle className="subtitle-card">Explora Antioquia</CardSubtitle>
                                <CardText className="text-card">Registra los proveedores de actividades </CardText>
                                <div className="box-btn-see-detail">
                                    <button type="button" className="button-card-footer" >INGRESAR<span class="material-icons icon-button">arrow_forward</span></button>
                                </div>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardImg top width="100%" className="img-card-index" src={require("assets/img/img30.jpg")} alt="Card image cap" />
                            <CardBody>
                                <CardTitle className="title-card"><span className="material-icons icono-card">hotel</span>Alojamiento</CardTitle>
                                <CardSubtitle className="subtitle-card">Experimenta Antioquia</CardSubtitle>
                                <CardText className="text-card">Registrar alojamietos</CardText>
                                <div className="box-btn-see-detail">
                                    <button type="button" className="button-card-footer" >INGRESAR<span class="material-icons icon-button">arrow_forward</span></button>
                                </div>
                            </CardBody>
                        </Card>
                    </CardDeck>
                </Container>
            </>
        );
    }
}

export default index;