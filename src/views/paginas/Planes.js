/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable default-case */
import React from "react";
import PlaneDatos from "views/paginas/PlanesDatos.js";
import Planesalida from "views/paginas/Planesalida.js";
import Planesalimentacion from "views/paginas/Planesalimentacion.js";
import Planestransporte from "views/paginas/Planestransporte.js";
import Planesalojamiento from "views/paginas/Planealojamiento.js";
import Planesactividades from "views/paginas/Planesactividad.js";
import Planessubmit from "views/paginas/Planessubmit.js";
export default class Planes extends React.Component {

    state = {
        step: 1,
        nombrePlan: '',
        ciudadOrigen: '',
        ciudadDestino: '',
        tipoPlan: '',
        descripcion: '',
        codigoSalida: '',
        codigoAlimentacion: '',
        itemDesayuno: '',
        itemAlmuerzo: '',
        itemCena: '',
        precioDesayuno:'',
        precioAlmuerzo:'',
        precioCena: '',
        informacionAdicionalAlimentacion: '',
        codigoTransporte: '',
        tipoVehiculo: '',
        precioPasaje: '',
        otroConductor: '',
        wifiAbordo: '',
        wcAbordo: '',
        paradas: '',
        descripcionVehiculo: '',
        codigoAlojamiento: '',
        tipoHabitacion: '',
        capacidadHabitacion: '',
        precioHabitacion: '',
        wifiHabitacion: '',
        servicioHabitacion: '',
        cajaFuerteHabitacion: '',
        informacionAdicionalAlojamiento: '',
        codigoActividad: '',
        precio: ''
    }

    // Procedd next step
    nextStep = () => {
        const { step } = this.state;
        this.setState({
            step: step + 1
        });
    }
    // Procedd back step
    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    }
    //handle fields change 
    handleChange = input => e => {
        this.setState({ [input]: e.target.value });
    }


    render() {
        const { step } = this.state;
        const { nombrePlan,
        ciudadOrigen,
        ciudadDestino,
        tipoPlan,
        descripcion,
        codigoSalida,
        codigoAlimentacion,
        itemDesayuno,
        itemAlmuerzo,
        itemCena,
        precioDesayuno,
        precioAlmuerzo,
        precioCena,
        informacionAdicionalAlimentacion,
        codigoTransporte,
        tipoVehiculo,
        precioPasaje,
        otroConductor,
        wifiAbordo,
        wcAbordo,
        paradas,
        descripcionVehiculo,
        codigoAlojamiento,
        tipoHabitacion,
        capacidadHabitacion,
        precioHabitacion,
        wifiHabitacion,
        servicioHabitacion,
        cajaFuerteHabitacion,
        informacionAdicionalAlojamiento,
        codigoActividad,
        precio} = this.state;

        const values = {
        nombrePlan,
        ciudadOrigen,
        ciudadDestino,
        tipoPlan,
        descripcion,
        codigoSalida,
        codigoAlimentacion,
        itemDesayuno,
        itemAlmuerzo,
        itemCena,
        precioDesayuno,
        precioAlmuerzo,
        precioCena,
        informacionAdicionalAlimentacion,
        codigoTransporte,
        tipoVehiculo,
        precioPasaje,
        otroConductor,
        wifiAbordo,
        wcAbordo,
        paradas,
        descripcionVehiculo,
        codigoAlojamiento,
        tipoHabitacion,
        capacidadHabitacion,
        precioHabitacion,
        wifiHabitacion,
        servicioHabitacion,
        cajaFuerteHabitacion,
        informacionAdicionalAlojamiento,
        codigoActividad,
        precio
        }

        switch (step) {
            case 1:
                return (
                    <PlaneDatos
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={values}
                    />
                )
            case 2:
                return (
                    <Planesalida
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                    />
                )
            case 3:
                return (
                    <Planesalimentacion
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                        handleChangetwo = {this.handleChangetwo}
                    />
                )

            case 4:
                return (
                    <Planestransporte
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                    />
                )

            case 5:
                return (
                    <Planesalojamiento
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                    />
                )

            case 6:
                return (
                    <Planesactividades
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                    />
                )

            case 7:
                return (
                    <Planessubmit
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        values={values}
                    />
                )
        }
    }
}
