/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import CabezotePlanes from "components/header/Headerplanes";
import crearPlan from "services/CrearPlan.js";
import { Growl } from 'primereact/growl';
export default class Planessubmit extends React.Component {

    constructor() {
        super();
        this.state = {
            visibleCard: false,
            plan: {
                nombre: null,
                precio: null,
                origen: null,
                destino: null,
                descripcion: null,
                tipo: null,
                salida: {
                    id: null
                },
                transporte: {
                    id: null
                },
                detalles_transporte: null,

                alimentacion: {
                    id: null
                },
                detalles_alimentacion: null,

                alojamiento: {
                    id: null
                },
                detalles_alojamiento: null,

                actividades: {
                    id: null
                }
            }
        };
        this.crearplan = new crearPlan();
        this.CrearPlan = this.CrearPlan.bind(this);
    }

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }

    CrearPlan() {
        this.crearplan.crearviaje(this.state.plan).then(data => {
            console.log(data);
            this.setState = {
                visibleCard: false,
                plan: {
                    nombre: null,
                    precio: null,
                    origen: null,
                    destino: null,
                    descripcion: null,
                    tipo: null,
                    salida: {
                        id: null
                    },
                    transporte: {
                        id: null
                    },
                    detalles_transporte: null,

                    alimentacion: {
                        id: null
                    },
                    detalles_alimentacion: null,

                    alojamiento: {
                        id: null
                    },
                    detalles_alojamiento: null,

                    actividades: {
                        id: null
                    }
                }
            }
        });
        this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'se creo el plan exitosamente' });
    }

    render() {
        const { values: { nombrePlan,
            ciudadOrigen,
            ciudadDestino,
            tipoPlan,
            descripcion,
            codigoSalida,
            codigoAlimentacion,
            itemDesayuno,
            itemAlmuerzo,
            itemCena,
            precioDesayuno,
            precioAlmuerzo,
            precioCena,
            informacionAdicionalAlimentacion,
            codigoTransporte,
            tipoVehiculo,
            precioPasaje,
            otroConductor,
            wifiAbordo,
            wcAbordo,
            paradas,
            descripcionVehiculo,
            codigoAlojamiento,
            tipoHabitacion,
            capacidadHabitacion,
            precioHabitacion,
            wifiHabitacion,
            servicioHabitacion,
            cajaFuerteHabitacion,
            informacionAdicionalAlojamiento,
            codigoActividad,
        } } = this.props;

        let precioalmuerzo, preciodesayuno, preciocena, preciopasaje, precioinpuestos, PrecioHabitacion, total;
        precioalmuerzo = precioAlmuerzo;
        preciodesayuno = precioDesayuno;
        preciocena = precioCena;
        preciopasaje = precioPasaje;
        precioinpuestos = 5800;
        PrecioHabitacion = precioHabitacion;
        PrecioHabitacion = (isNaN(parseInt(PrecioHabitacion))) ? 0 : parseInt(PrecioHabitacion);
        precioalmuerzo = (isNaN(parseInt(precioalmuerzo))) ? 0 : parseInt(precioalmuerzo);
        preciodesayuno = (isNaN(parseInt(preciodesayuno))) ? 0 : parseInt(preciodesayuno);
        preciocena = (isNaN(parseInt(preciocena))) ? 0 : parseInt(preciocena);
        preciopasaje = (isNaN(parseInt(preciopasaje))) ? 0 : parseInt(preciopasaje);
        total = preciodesayuno + precioalmuerzo + preciocena + preciopasaje + precioinpuestos + PrecioHabitacion;
        let pago = formatCurrency("es-CO", "COP", 2, total);
        console.log(pago);

        let descripcionAlimentacion;
        descripcionAlimentacion = "Alimentacion: " + itemDesayuno + ", precio:" + precioDesayuno + ", " + itemAlmuerzo + ", precio del almuerzo: " + precioAlmuerzo + "," +
            itemCena  + ", precio de la cena: " + precioCena + ", información adicional: " + informacionAdicionalAlimentacion;
        console.log(descripcionAlimentacion);
        let descripcionTransporte;
        descripcionTransporte = "Tipo de vehiculo: " + tipoVehiculo + ", precio del pasaje: " + precioPasaje + ", servicios: " + otroConductor + ", " + wifiAbordo + "," + wcAbordo + ", " + paradas + ",  información adicional: " + descripcionVehiculo;
        console.log(descripcionTransporte);
        let descripcionAlojamiento;
        descripcionAlojamiento = "tipo de habitación: " + tipoHabitacion + ", capacidad: " + capacidadHabitacion + " , servicios: " + wifiHabitacion + ", " + servicioHabitacion + ", " + cajaFuerteHabitacion + ",  información adicional: " + informacionAdicionalAlojamiento
            + " precio por persona/noche: " + precioHabitacion;
        console.log(descripcionAlojamiento);
        let salida, alimentacion, transporte, alojamiento, actividades;
        alimentacion = codigoAlimentacion;
        transporte = codigoTransporte;
        alojamiento = codigoAlojamiento;
        actividades = codigoActividad;
        salida = codigoSalida;
        salida = (isNaN(parseInt(salida))) ? 0 : parseInt(salida);
        alimentacion = (isNaN(parseInt(alimentacion))) ? 0 : parseInt(alimentacion);
        transporte = (isNaN(parseInt(transporte))) ? 0 : parseInt(transporte);
        alojamiento = (isNaN(parseInt(alojamiento))) ? 0 : parseInt(alojamiento);
        actividades = (isNaN(parseInt(actividades))) ? 0 : parseInt(actividades);
        console.log(salida);

        function formatCurrency(locales, currency, fractionDigits, number) {
            var formatted = new Intl.NumberFormat(locales, {
                style: 'currency',
                currency: currency,
                minimumFractionDigits: fractionDigits
            }).format(number);
            return formatted;
        }

        return (
            <div>
                <CabezotePlanes />
                <section className="container">
                    <div className="card-info-plan-total">
                        <div className="header-card-total-plan">
                            <label className="label-title-total-plan"
                                onLoad={this.state.plan.destino = ciudadDestino}
                            >Plan vacacional hacia {ciudadDestino}</label>
                        </div>
                        <div className="body-card-total-plan">
                            <form name="plan">
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.nombre = nombrePlan}>Nombre del plan</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {nombrePlan}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan" onLoad={this.state.plan.origen = ciudadOrigen}
                                            >Ciudad de origen</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {ciudadOrigen}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.destino = ciudadDestino}
                                            >Ciudad de destino</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {ciudadDestino}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.tipo = tipoPlan}
                                            >Tipo de plan</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {tipoPlan}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.descripcion = descripcion}
                                            >Descripcion del plan</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {descripcion}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.salida.id = salida}
                                            >Boleta de salida (Codigo)</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {salida}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.alimentacion.id = alimentacion}
                                            >Alimentación (codigo)</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {alimentacion}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.detalles_alimentacion = descripcionAlimentacion}
                                            >Detalles de la alimentación</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {descripcionAlimentacion}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.transporte.id = transporte}
                                            >Transporte (codigo)</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {transporte}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.detalles_transporte = descripcionTransporte}
                                            >Detalles de transporte</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {descripcionTransporte}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.alojamiento.id = alojamiento}
                                            >Alojamiento (codigo)</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {alojamiento}</label>

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.detalles_alojamiento = descripcionAlojamiento}
                                            >Detalles del alojamiento</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {descripcionAlojamiento}</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-plan"
                                                onLoad={this.state.plan.actividades.id = actividades}
                                            >Actividades (codigo)</label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="box-form">
                                            <label className="label-item-plan"> {actividades}</label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="footer-card-total-plan">
                            <div className="box-price">
                                <label className="label-tax-pay" name="preciotax" id="taxs" > impuestos: ${precioinpuestos}</label>
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <div className="box-price">
                                        <label className="label-total-pay">Precio del plan </label>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="box-form">
                                        <label className="label-item-total-plan"
                                            onLoad={this.state.plan.precio = total}
                                        >{pago}</label>
                                    </div>
                                </div>
                            </div>
                            <div className="box-btn-save-data">
                                <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                <button type="button" className="btn-save-information" onClick={this.CrearPlan} >Crear plan vacacional</button>
                            </div>
                        </div>
                    </div>
                </section>
                <Growl ref={(el) => this.growl = el} />
            </div>
        );
    }




}