/* eslint-disable no-unused-expressions */
/* eslint-disable no-this-before-super */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Button, FormGroup, Form, Input, InputGroupAddon, InputGroupText, InputGroup } from "reactstrap";
import axios from "axios";
class login extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <div className="col-6">
                    <img alt="imagen-bienvenida-consola-admin" className="img-welcome-login-admin" src={require("assets/img/3246635.jpg")} />
                </div>
                <div className="col-6">
                    <div className="card-login">
                        <div className="img-logo-login">
                            <img alt="Logo appuebliar" src={require("assets/img/logodosdos.png")} />
                        </div>
                        <div className="box-title-admin-appuebliar">
                            <h1 className="text-title-admin"> appuebliar | Administración</h1>
                        </div>
                        <Form role="form" id="formlogin">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <span className="material-icons">email</span>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="example@domain.com" type="email" autoComplete="new-email" id="email" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <span className="material-icons">lock</span>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="********" type="password" autoComplete="new-password" id="password" />
                                </InputGroup>
                            </FormGroup>
                            <div className="box-alert-msg" id="boxalerterror">
                                <label className="error" id="msgerror">Ingrese un correo y/o contraseña</label>
                            </div>
                            <div className="box-user-alert" id="boxalert">
                                <label className="alert" id="msgalert">Este usuario no existe</label>
                            </div>
                            <div className="text-center">
                                <Button className="my-4 button-login" color="appuebliarcoloruno" id="btn-login" type="button" onClick={this.login}>
                                    <span className="label-btn-login">Ingresar</span>
                                </Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </>
        );
    }


    login() {
        var apiBaseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/admin/correo/";
        var msgerror = document.getElementById('boxalerterror');
        var msgalert = document.getElementById('boxalert');
        var email;
        var password;
        email = document.getElementById('email');
        password = document.getElementById('password');
        if (email.value === '' || password.value === '') {
                msgerror.style.display = "block";
        } else {
            axios.get(apiBaseUrl + email.value).then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    if (email.value === response.data.correo && password.value === response.data.password) {
                        window.location.href = "/admin/index.js";
                    } else if (email.value !== response.data.correo || password.value !== response.data.password) {
                        msgerror.style.display = "none";
                        msgalert.style.display = "block";
                    }
                }
            })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
}

export default login