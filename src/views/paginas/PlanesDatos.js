import React from "react";
import CabezotePlanes from "components/header/Headerplanes";

export default class PlanesDatos extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }
    render() {
        const { values, handleChange } = this.props;

        return (
            <div>
                <CabezotePlanes />
                <section className="container">
                    <div className="card-principal">
                        <div className="header-card">
                            <label className="label-title-name-plan"> Crear plan turistico</label>
                        </div>
                        <div className="body-card">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/4221492.jpg")} alt="banner imagen nuevo plan" />
                                </div>
                                <div className="col-6">
                                    <form id="formInitial" name="forminitial">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="box-input-plan">
                                                    <label className="label-form-plan">Nombre del plan</label>
                                                    <input className="input-form-plan" type="text" onChange={handleChange('nombrePlan')} defaultValue={values.nombrePlan} placeholder="Ponle nombre al plan"></input>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <div className="box-input-plan">
                                                    <label className="label-form-plan">Ciudad de origen</label>
                                                    <input className="input-form-plan" type="text" onChange={handleChange('ciudadOrigen')} defaultValue={values.ciudadOrigen} placeholder="Ingrese el origen"></input>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="box-input-plan">
                                                    <label className="label-form-plan">Ciudad de destino</label>
                                                    <input className="input-form-plan" type="text" onChange={handleChange('ciudadDestino')} defaultValue={values.ciudadDestino} placeholder="Ingrese el destino"></input>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-8">
                                                <div className="box-input-plan">
                                                    <label className="label-form-plan">Tipo de plan</label>
                                                    <select name="select" className="select-type-plan" onChange={handleChange('tipoPlan')} defaultValue={values.tipoPlan}>
                                                        <option value="todo incluido">Todo incluido</option>
                                                        <option value="Dia de sol">Dia de sol</option>
                                                        <option value="Amanecida">Amanecida</option>
                                                        <option value="Familiar">Familiar</option>
                                                        <option value="Diversion">Diversion</option>
                                                        <option value="Ecologico">Ecologico</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="box-input-plan">
                                                    <label className="label-form-plan">Ingrese una descripción</label>
                                                    <textarea name="textarea" className="input-form-plan-description" rows="3" cols="50" onChange={handleChange('descripcion')} defaultValue={values.descripcion} placeholder="Mas información, detalles o un itinerario"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="footer-card">
                                <div className="box-btn-save-data">
                                    <button type="button" className="btn-save-information" onClick={this.continue}>Guardar y continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}



