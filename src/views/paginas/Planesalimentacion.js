/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Dialog } from 'primereact/dialog';
import CabezotePlanes from "components/header/Headerplanes";
import axios from "axios";
import FormInputProvider from "components/forms/ProviderTransport.js";
import { Button } from 'primereact/button';
export default class Planesalimentacion extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const { values, handleChange } = this.props;
        return (
            <div>
                <CabezotePlanes />
                <section className="container">
                    <div className="card-food-plan">
                        <div className="card-header-date">
                            <label className="label-title-name-plan">Alimentación</label>
                        </div>
                        <div className="body-card">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/3907208.jpg")} alt="banner imagen alimentacion" />
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-title-item">
                                                <label className="label-item">Agregar restaurante</label>
                                            </div>
                                            <div className="box-input-plan">
                                                <label className="label-form-item">Ingrese el codigo</label>
                                                <input className="input-form-plan" type="text" id="codigoProviderFood" name="codigoFoodPorvider" onChange={handleChange('codigoAlimentacion')} defaultValue={values.codigoAlimentacion} placeholder="Codigo del restaurante"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-btn-validate">
                                                <button type="button" className="btn-validate-item" onClick={this.validateProvider}>Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                        </div>
                                    </div>
                                    <div className="container">
                                        <div className="card-result-item" id="cardResult">
                                            <div className="row">
                                                <div className="col-4">
                                                    <img className="img-banner-plan" src={require("assets/img/menu.png")} alt="error" />
                                                </div>
                                                <div className="col-8">
                                                    <div className="header-card-result">
                                                        <label className="label-title-result">Establecimiento - codigo <label className="label-result-item-codex" id="codigo" onChange={handleChange('codigoAlimentacion')} defaultValue={values.codigoAlimentacion}></label> </label>
                                                    </div>
                                                    <div className="body-card-result">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="box-name-provider">
                                                                    <label className="label-title-provider" id="nombre" onChange={handleChange('nombreRestaurante')} defaultValue={values.nombreRestaurante}></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="direccion" className="label-result-item" onChange={handleChange('direccionRestaurante')} defaultValue={values.direccionRestaurante}></label>
                                                                </div>
                                                            </div>
                                                            <div className="col-4">
                                                                <div className="box-item-provider">
                                                                    <label id="contacto" className="label-result-item" onChange={handleChange('telefonoRestaurante')} defaultValue={values.telefonoRestaurante}></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-7">
                                                                <div className="box-item-provider">
                                                                    <label id="ubicacion" className="label-city-provider" onChange={handleChange('ciudadRestaurante')} defaultValue={values.ciudadRestaurante}></label>
                                                                </div>
                                                            </div>
                                                            <div className="col-5">
                                                                <div className="box-item-provider">
                                                                    <label id="capacidad" className="label-result-item" onChange={handleChange('capacidadRestaurante')} defaultValue={values.capacidadRestaurante}></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-result-item-error" id="carderror">
                                            <div className="body-card-result">
                                                <div className="row">
                                                    <div className="col-4">
                                                        <img className="img-banner-plan" src={require("assets/img/error.png")} alt="error" />
                                                    </div>
                                                    <div className="col-8">
                                                        <div className="box-error">
                                                            <label id="errormsg" className="label-error-msg" id="labelerror"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div className="card-detail-item">
                                <div className="box-title-item-provider">
                                    <h1 className="title-item-provider-food">Agregar detalles</h1>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye desayuno?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('itemDesayuno')} onClick={this.habilitarPreciosDesayuno} defaultValue={values.itemDesayuno} id="itemDesayuno">
                                                <option>Seleccione una opcion</option>
                                                <option value="Desayuno incluido">SI</option>
                                                <option value="Desayuno no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye almuerzo?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('itemAlmuerzo')} onClick={this.habilitarPreciosAlmuerzo} defaultValue={values.itemAlmuerzo} id="itemAlmuerzo">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="Almuerzo incluido" >SI</option>
                                                <option value="Almuerzo no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye cena?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('itemCena')} onClick={this.habilitarPreciosCena} defaultValue={values.itemCena} id="itemCena">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="Cena incluida">SI</option>
                                                <option value="Cena no incluida">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Precio del desayuno</label>
                                            <input className="input-form-plan" type="text" onChange={handleChange('precioDesayuno')} defaultValue={values.precioDesayuno}
                                                placeholder="Ingrese un valor valido" disabled={true} id="inputDesayuno"></input>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Precio del almuerzo</label>
                                            <input className="input-form-plan" type="text" onChange={handleChange('precioAlmuerzo')} defaultValue={values.precioAlmuerzo}
                                                placeholder="Ingrese un valor valido" disabled={true} id="inputAlmuerzo"></input>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Precio de la cena</label>
                                            <input className="input-form-plan" type="text" onChange={handleChange('precioCena')} defaultValue={values.precioCena} 
                                            placeholder="Ingrese un valor valido" disabled={true} id="inputCena"></input>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="box-input-plan-description">
                                            <label className="label-form-plan-description">Informacion adicional</label>
                                            <textarea name="textarea" className="input-form-plan-description" rows="3" cols="50" onChange={handleChange('informacionAdicionalAlimentacion')} defaultValue={values.informacionAdicionalAlimentacion} placeholder="Incluir licor, refrigerios o snacks..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="footer-card">
                                <div className="box-btn-save-data">
                                    <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                    <button type="button" className="btn-save-information" onClick={this.continue}>Guardar y continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

    validateProvider() {
        var urlProvider = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/proveedoralimentacion/";
        var codexProviderFood;
        codexProviderFood = document.getElementById('codigoProviderFood');
        console.log(codexProviderFood.value);
        axios.get(urlProvider + codexProviderFood.value).then(function (response) {
            console.log(response);
            console.log(response.data);
            if (response.data.response === "BLANK") {
                var box = document.getElementById('carderror');
                box.style.display = "block";
                var div = document.getElementById('cardResult');
                div.style.display = "none";
                document.getElementById('labelerror').innerHTML = "Lo sentimos, este proveedor no existe :(";
            } else {
                var div = document.getElementById('cardResult');
                div.style.display = "block";
                var box = document.getElementById('carderror');
                box.style.display = "none";
                document.getElementById('codigo').innerHTML = response.data.id;
                document.getElementById('nombre').innerHTML = response.data.nombreEstablecimiento;
                document.getElementById('ubicacion').innerHTML = response.data.ubicacion;
                document.getElementById('direccion').innerHTML = response.data.direccion;
                document.getElementById('capacidad').innerHTML = response.data.capacidadAtencion + " persona(s)";
                document.getElementById('contacto').innerHTML = response.data.contacto;
            }
        })
    }

    habilitarPreciosDesayuno() {
        var opcion = document.getElementById('itemDesayuno');
        if (opcion.value === "Desayuno incluido") {
            document.getElementById('inputDesayuno').disabled = false;
        } else {
            document.getElementById('inputDesayuno').disabled = true;
            document.getElementById('inputDesayuno').value = 0;
        }
    }

    habilitarPreciosAlmuerzo() {
        var opcion = document.getElementById('itemAlmuerzo');
        if (opcion.value === "Almuerzo incluido") {
            document.getElementById('inputAlmuerzo').disabled = false;
        } else {
            document.getElementById('inputAlmuerzo').disabled = true;
            document.getElementById('inputAlmuerzo').value = "";
        }
    }

    habilitarPreciosCena() {
        var opcion = document.getElementById('itemCena');
        if (opcion.value === "Cena incluido") {
            document.getElementById('inputCena').disabled = false;
        } else {
            document.getElementById('inputCena').disabled = true;
            document.getElementById('inputCena').value = "";
        }
    }
}