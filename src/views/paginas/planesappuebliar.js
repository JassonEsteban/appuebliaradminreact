import React from "react";
import Headerplan from "components/header/HeaderCrudPlanes.js";

export default class planesappuebliar extends React.Component {

  render() {
    return (
      <div>
        <Headerplan />
        <section className="container-fluid">
          <div className="box-title">
            <label className="title-plan">Planes disponibles</label>
          </div>

          <div className="body-section">
            <div className="row">
              <div className="col-4">
                <div className="card-plan">
                  <div className="head-image-card">
                    <img className="img-plan" src={require("assets/img/img20.jpg")} alt="imagen-referencia-plan"></img>
                  </div>
                  <div className="container-title-plan">
                    <label className="label-title-plan">Nombre del plan creado</label>
                  </div>
                  <div className="body-card-plan">
                    <div className="row">
                      <div className="col-6">
                        <div className="box-item">
                          <div className="label-card">Ciudad de origen</div>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="box-item-two">
                          <div className="label-card">20/20/2020</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="container-footer-card">
                      <div className="box-title-actions">
                        <span className="title-actions">Acciones</span>
                      </div>
                      <div className="box-action">
                        <div className="row">
                          <div className="col-4">
                            <div className="box-btn">
                              <button className="btn-action-edit" type="button">Editar</button>
                            </div>
                          </div>
                          <div className="col-4">
                            <div className="box-btn">
                              <button className="btn-action-delete" type="button">Eliminar</button>
                            </div>
                          </div>
                          <div className="col-4">
                            <div className="box-btn">
                              <button className="btn-action-information" type="button">Ver plan</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
