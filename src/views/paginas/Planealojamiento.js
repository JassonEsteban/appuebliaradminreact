/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import CabezotePlanes from "components/header/Headerplanes";
import axios from "axios";
export default class Planesalojamiento extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const { values, handleChange } = this.props;
        return (
            <div>
                <CabezotePlanes />
                <section className="container">
                    <div className="card-transport-plan">
                        <div className="card-header-date">
                            <label className="label-title-name-plan">Hospedaje</label>
                        </div>
                        <div className="body-card">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/4238851.jpg")} alt="banner imagen hospedaje" />
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-title-item">
                                                <label className="label-item">Agregar alojamiento</label>
                                            </div>
                                            <div className="box-input-plan">
                                                <label className="label-form-item">Ingrese el codigo</label>
                                                <input className="input-form-plan" type="text" id="codigoAlojamientoProvider" name="codigoAlojamiento" onChange={handleChange('codigoAlojamiento')} defaultValue={values.codigoAlojamiento} placeholder="Codigo del hotel"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-btn-validate">
                                                <button type="button" className="btn-validate-item" onClick={this.validateProvider}>Agregar</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="container">
                                        <div className="card-result-item" id="cardResult">
                                            <div className="row">
                                                <div className="col-4">
                                                    <img className="img-banner-plan" src={require("assets/img/alojamiento.png")} alt="error" />
                                                </div>
                                                <div className="col-8">
                                                    <div className="header-card-result">
                                                        <label className="label-title-result">Alojamiento <label className="label-result-item-codex" id="codigo" onChange={handleChange('codigoAlojamiento')} defaultValue={values.codigoAlojamiento}></label> </label>
                                                    </div>
                                                    <div className="body-card-result">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="box-name-provider">
                                                                    <label className="label-title-provider" id="nombre"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="direccion" className="label-result-item" ></label>
                                                                </div>
                                                            </div>
                                                            <div className="col-4">
                                                                <div className="box-item-provider">
                                                                    <label id="contacto" className="label-result-item" ></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="ubicacion" className="label-city-provider"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-result-item-error" id="carderror">
                                            <div className="body-card-result">
                                                <div className="row">
                                                    <div className="col-4">
                                                        <img className="img-banner-plan" src={require("assets/img/error.png")} alt="error" />
                                                    </div>
                                                    <div className="col-8">
                                                        <div className="box-error">
                                                            <label id="errormsg" className="label-error-msg" id="labelerror"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card-detail-item">
                                <div className="box-title-item-provider">
                                    <h1 className="title-item-provider-food">Agregar detalles</h1>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Tipo de habitación</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('tipoHabitacion')} defaultValue={values.tipoHabitacion} id="tipoVehiculo">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="Habitación estandar">Estandar</option>
                                                <option value="Habitación para parejas">Parejas</option>
                                                <option value="Habitación premium">Premium</option>
                                                <option value="Habitación familiar">Familiar</option>
                                                <option value="No incluye habitación">No incluye</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Capacidad de la habitación</label>
                                            <input className="input-form-plan" type="text" onChange={handleChange('capacidadHabitacion')} defaultValue={values.capacidadHabitacion} placeholder="Ingrese el valor"></input>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Precio por noche/persona</label>
                                            <input className="input-form-plan" type="text" onChange={handleChange('precioHabitacion')} defaultValue={values.precioHabitacion} placeholder="Ingrese el valor"></input>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿incluye wifi?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('wifiHabitacion')} defaultValue={values.wifiHabitacion} id="wifiHabitacion">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="wifi en la habitación">SI</option>
                                                <option value="wifi no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye servicio a la habitación?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('servicioHabitacion')} defaultValue={values.servicioHabitacion} id="servicioHabitacion">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="Servicio a la habitación">SI</option>
                                                <option value="Servicio a la habitación no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye caja fuerte?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('cajaFuerteHabitacion')} defaultValue={values.cajaFuerteHabitacion} id="cajaFuerteHabitacion">
                                                <option value="todo incluido">Seleccione una opcion</option>
                                                <option value="caja fuerte en la habitación">SI</option>
                                                <option value="caja fuerte en la habitación no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="box-input-plan-description">
                                            <label className="label-form-plan-description">Informacion adicional</label>
                                            <textarea name="textarea" className="input-form-plan-description" rows="3" cols="50" onChange={handleChange('informacionAdicionalAlojamiento')} defaultValue={values.informacionAdicionalAlojamiento} placeholder="Descripcion breve de la habitacion, horarios y precauciones..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div className="footer-card">
                                <div className="box-btn-save-data">
                                    <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                    <button type="button" className="btn-save-information" onClick={this.continue}>Guardar y continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
    /*

    validateProvider() {
        var div = document.getElementById('cardResult');
        div.style.display = "block";
        var urlProvider = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/proveedoralojamiento/";
        var codexProviderFood;
        codexProviderFood = document.getElementById('codigoAlojamientoProvider');
        console.log(codexProviderFood.value);
        axios.get(urlProvider + codexProviderFood.value).then(function (response) {
            console.log(response);
            console.log(response.data);
            document.getElementById('codigo').innerHTML = response.data.id;
            document.getElementById('nombre').innerHTML = response.data.nombre;
            document.getElementById('ubicacion').innerHTML = response.data.ubicacion;
            document.getElementById('direccion').innerHTML = response.data.direccion;
            document.getElementById('contacto').innerHTML = response.data.contacto;
        })
    } */

    validateProvider() {
        var urlProvider = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/proveedoralojamiento/";
        var codexProviderFood;
        codexProviderFood = document.getElementById('codigoAlojamientoProvider');
        console.log(codexProviderFood.value);
        axios.get(urlProvider + codexProviderFood.value).then(function (response) {
            console.log(response);
            console.log(response.data);
            if (response.data.response === "BLANK") {
                var box = document.getElementById('carderror');
                box.style.display = "block";
                var div = document.getElementById('cardResult');
                div.style.display = "none";
                document.getElementById('labelerror').innerHTML = "Lo sentimos, este proveedor no existe :(";
            } else {
                var div = document.getElementById('cardResult');
                div.style.display = "block";
                var box = document.getElementById('carderror');
                box.style.display = "none";
                document.getElementById('codigo').innerHTML = response.data.id;
                document.getElementById('nombre').innerHTML = response.data.nombre;
                document.getElementById('ubicacion').innerHTML = response.data.ubicacion;
                document.getElementById('direccion').innerHTML = response.data.direccion;
                document.getElementById('contacto').innerHTML = response.data.contacto;
            }
        })
    }
}