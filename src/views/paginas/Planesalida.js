import React from "react";
import CabezotePlanes from "components/header/Headerplanes";
import CrearSalida from "services/CrearSalida.js";
export default class Planesalida extends React.Component {
    constructor() {
        super();
        this.state = {
            visibleCard: false,
            salida: {
                descripcion: null,
                lugarSalida: null,
                fechaSalida: null,
                fechaVencimiento: null,
                horaSalida: null,
            }
        };
        this.crearsalida = new CrearSalida();
        this.GuardarSalida = this.GuardarSalida.bind(this);
    }


    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }



    render() {
        const { values, handleChange } = this.props;
        return (
            <div>
                <CabezotePlanes />
                <section className="container">

                    <div className="card-exit" id="cardsuccessexit">
                        <div className="card-header-date">
                            <label className="label-title-name-plan">Establecer salida</label>
                        </div>
                        <div className="body-card">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/3573382.jpg")} alt="banner imagen calendario" />
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-success">
                                                <label className="label-success">EXCELENTE</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-info-success">
                                                <label className="label-info-success">La salida fue programada con exito</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-card-exit">
                                                <label className="boleto-exit">Boleto de salida</label>
                                                <div className="row">
                                                    <div className="box-number">
                                                        <input type="text" className="input-codex" onClick={handleChange('codigoSalida')} value={values.codigoSalida} id="codexitemexit" ></input> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="footer-card">
                            <div className="box-btn-save-data">
                                <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                <button type="button" className="btn-save-information" onClick={this.continue}>Guardar y continuar</button>
                            </div>
                        </div>
                    </div>



                    <div className="card-date-plan" id="cardexitplan">
                        <div className="card-header-date">
                            <label className="label-title-name-plan">Establecer la salida</label>
                        </div>
                        <div className="body-card" id="boxexit">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/3573382.jpg")} alt="banner imagen calendario" />
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="box-input-plan">
                                                <label className="label-form-date">Fecha de salida</label>
                                                <input className="input-form-plan" type="date" placeholder="Seleccione una fecha" id="fechasalida"
                                                    value={this.state.salida.fechaSalida || ''} onChange={(e) => {
                                                        let val = e.target.value;
                                                        this.setState(prevState => {
                                                            let salida = Object.assign({}, prevState.salida);
                                                            salida.fechaSalida = val;
                                                            return { salida };
                                                        })
                                                    }} />
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="box-input-plan">
                                                <label className="label-form-date">Fecha de regreso</label>
                                                <input className="input-form-plan" type="date" placeholder="Seleccione una fecha" id="fecharegreso"
                                                    value={this.state.salida.fechaVencimiento || ''} onChange={(e) => {
                                                        let val = e.target.value;
                                                        this.setState(prevState => {
                                                            let salida = Object.assign({}, prevState.salida);
                                                            salida.fechaVencimiento = val;
                                                            return { salida };
                                                        })
                                                    }} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="box-input-plan">
                                                <label className="label-form-date">Hora de salida</label>
                                                <input className="input-form-plan" type="time" placeholder="Seleccione una hora" id="horasalida"
                                                    value={this.state.salida.horaSalida || ''} onChange={(e) => {
                                                        let val = e.target.value;
                                                        this.setState(prevState => {
                                                            let salida = Object.assign({}, prevState.salida);
                                                            salida.horaSalida = val;
                                                            return { salida };
                                                        })
                                                    }} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-input-plan">
                                                <label className="label-form-plan">Lugar de salida</label>
                                                <input className="input-form-plan" type="text" placeholder="Ingrese el lugar de abordaje" id="lugarsalida"
                                                    value={this.state.salida.lugarSalida || ''} onChange={(e) => {
                                                        let val = e.target.value;
                                                        this.setState(prevState => {
                                                            let salida = Object.assign({}, prevState.salida);
                                                            salida.lugarSalida = val;
                                                            return { salida };
                                                        })
                                                    }} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-input-plan">
                                                <label className="label-form-plan">Información adicional</label>
                                                <input className="input-form-plan" type="textarea" id="informacionadicional" placeholder="Indicaciones, recomendaciones, descripciones..."
                                                    value={this.state.salida.descripcion || ''} onChange={(e) => {
                                                        let val = e.target.value;
                                                        this.setState(prevState => {
                                                            let salida = Object.assign({}, prevState.salida);
                                                            salida.descripcion = val;
                                                            return { salida };
                                                        })
                                                    }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="footer-card">
                                <div className="box-btn-save-data">
                                    <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                    <button type="button" className="btn-save-information" onClick={this.GuardarSalida}>Registrar salida
                                    <div id="loading"></div></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

    GuardarSalida() {
        var loading = document.getElementById('loading');
        loading.style.display = "block";
        var cardExit = document.getElementById('cardsuccessexit');
        var cardDate = document.getElementById('cardexitplan');
        this.crearsalida.createExit(this.state.salida).then(data => {
            console.log(data);
            this.setState({
                visibleCard: false,
                salida: {
                    descripcion: null,
                    lugarSalida: null,
                    fechaSalida: null,
                    fechaVencimiento: null,
                    horaSalida: null,
                },
            });
            cardExit.style.display = "block";
            loading.style.display = "none";
            cardDate.style.display = "none";
            document.getElementById('codexitemexit').value = data.id;
        })
    }
}