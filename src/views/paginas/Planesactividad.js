/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import CabezotePlanes from "components/header/Headerplanes";
import axios from "axios";
export default class Planesactividad extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const { values, handleChange } = this.props;
        return (
            <div>
                <CabezotePlanes />
                <section className="container">
                    <div className="card-transport-plan">
                        <div className="card-header-date">
                            <label className="label-title-name-plan">Actividades</label>
                        </div>
                        <div className="body-card">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/3470597.jpg")} alt="banner imagen actividades" />
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-title-item">
                                                <label className="label-item">Agregar actividades</label>
                                            </div>
                                            <div className="box-input-plan">
                                                <label className="label-form-item">Ingrese el codigo</label>
                                                <input className="input-form-plan" type="text" id="codigoActividadProvider" name="codigoActividad" onChange={handleChange('codigoActividad')} defaultValue={values.codigoActividad} placeholder="Codigo de la actividad"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-btn-validate">
                                                <button type="button" className="btn-validate-item" onClick={this.validateProvider}>Agregar</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="container">
                                        <div className="card-result-item" id="cardResult">
                                            <div className="row">
                                                <div className="col-4">
                                                    <img className="img-banner-plan" src={require("assets/img/excursionismo.png")} alt="error" />
                                                </div>
                                                <div className="col-8">
                                                    <div className="header-card-result">
                                                        <label className="label-title-result">Actividad <label className="label-result-item-codex" id="codigo" onChange={handleChange('codigoActividad')} defaultValue={values.codigoActividad}></label> </label>
                                                    </div>
                                                    <div className="body-card-result">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="box-name-provider">
                                                                    <label className="label-title-provider" id="nombre"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="direccion" className="label-result-item" ></label>
                                                                </div>
                                                            </div>
                                                            <div className="col-4">
                                                                <div className="box-item-provider">
                                                                    <label id="contacto" className="label-result-item" ></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="ubicacion" className="label-city-provider"></label>
                                                                </div>
                                                            </div>
                                                            <div className="col-4">
                                                                <div className="box-item-provider">
                                                                    <label id="capacidad" className="label-result-item"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-result-item-error" id="carderror">
                                            <div className="body-card-result">
                                                <div className="row">
                                                    <div className="col-4">
                                                        <img className="img-banner-plan" src={require("assets/img/error.png")} alt="error" />
                                                    </div>
                                                    <div className="col-8">
                                                        <div className="box-error">
                                                            <label id="errormsg" className="label-error-msg" id="labelerror"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            <div className="footer-card">
                                <div className="box-btn-save-data">
                                    <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                    <button type="button" className="btn-save-information" onClick={this.continue}>Guardar y continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

    /*
    validateProvider() {
        var div = document.getElementById('cardResult');
        div.style.display = "block";
        var urlProvider = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/actividad/";
        var codexProviderFood;
        codexProviderFood = document.getElementById('codigoActividadProvider');
        console.log(codexProviderFood.value);
        axios.get(urlProvider + codexProviderFood.value).then(function (response) {
            console.log(response);
            console.log(response.data);
            document.getElementById('codigo').innerHTML = response.data.id;
            document.getElementById('nombre').innerHTML = response.data.nombreActividad;
            document.getElementById('ubicacion').innerHTML = response.data.ubicacion;
            document.getElementById('direccion').innerHTML = response.data.lugarActividad;
            document.getElementById('capacidad').innerHTML = response.data.capacidadPersonas + " persona(s)";
            document.getElementById('contacto').innerHTML = response.data.contacto;
        })
    } */

    validateProvider() {
        var urlProvider = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/actividad/";
        var codexProviderFood;
        codexProviderFood = document.getElementById('codigoActividadProvider');
        console.log(codexProviderFood.value);
        axios.get(urlProvider + codexProviderFood.value).then(function (response) {
            console.log(response);
            console.log(response.data);
            if (response.data.response === "BLANK") {
                var box = document.getElementById('carderror');
                box.style.display = "block";
                var div = document.getElementById('cardResult');
                div.style.display = "none";
                document.getElementById('labelerror').innerHTML = "Lo sentimos, este proveedor no existe :(";
            } else {
                var div = document.getElementById('cardResult');
                div.style.display = "block";
                var box = document.getElementById('carderror');
                box.style.display = "none";
                document.getElementById('codigo').innerHTML = response.data.id;
                document.getElementById('nombre').innerHTML = response.data.nombreActividad;
                document.getElementById('ubicacion').innerHTML = response.data.ubicacion;
                document.getElementById('direccion').innerHTML = response.data.lugarActividad;
                document.getElementById('capacidad').innerHTML = response.data.capacidadPersonas + " persona(s)";
                document.getElementById('contacto').innerHTML = response.data.contacto;
            }
        })
    }
}