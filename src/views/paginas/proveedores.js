/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
/* eslint-disable no-unused-labels */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react/require-render-return */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import HeaderUser from "components/header/HeaderUsuario.js"; import { Container } from "reactstrap";
import Providerfood from "services/ProviderFood.js";
import { DataTable } from "primereact/datatable";
import MenuFood from "components/InsertForms/MenuFood.js";
import { Column } from "primereact/column";
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { Growl } from 'primereact/growl';
import { Menubar } from 'primereact/menubar';
import { TabView, TabPanel } from 'primereact/tabview';
import FormInputProvider from "components/forms/ProviderTransport.js";
import FormInputProviderHotel from "components/forms/ProviderHotel.js";
import FormInputProviderActivity from "components/forms/ProviderActivity.js";
import "primereact/resources/themes/nova-dark/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "../../assets/js/tabpanel.js";

class proveedores extends React.Component {
    constructor() {
        super();
        this.tabs__item = React.createRef();
        this.panels__item = React.createRef();
        this.state = {
            visibleCard: false,
            alimentacion: {
                nit: null,
                nombreEstablecimiento: null,
                contacto: null,
                ubicacion: null,
                direccion: null,
                descripcion: null,
                capacidadAtencion: null,
                urlWeb: null,
                puntospueblo: null,
                admin: {
                    nickName: null
                }
            },
            selectedProviderFood: {
            }
        };
        this.items = [
            {
                label: 'Registrar',
                icon: 'green pi pi-fw pi-plus',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Editar proveedor',
                icon: 'pi pi-fw pi-pencil',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Eliminar',
                icon: 'pi pi-fw pi-trash',
                command: () => { this.delete() }
            },
            {
                label: 'ver proveedor',
                icon: 'pi pi-fw pi-eye',
            }
        ];
        this.providerFood = new Providerfood();
        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.footer = (
            <div className="box-footer">
                <button className="button-add-provider" onClick={this.save} >Agregar proveedor</button>
            </div>
        );
    }
    componentDidMount() {
        this.providerFood.getAllFood().then(data => this.setState({ providerFood: data }))
    }
    save() {
        var nit, establecimiento, contacto, ubicacion, direccion, descripcion, capacidad, urlweb, puntospueblo, admin;
        nit = document.getElementById('nit');
        establecimiento = document.getElementById('nameProvider');
        contacto = document.getElementById('phoneProvider');
        ubicacion = document.getElementById('cityProvider');
        direccion = document.getElementById('addressProvider');
        descripcion = document.getElementById('description');
        capacidad = document.getElementById('capacidad');
        urlweb = document.getElementById('urlweb');
        puntospueblo = document.getElementById('pointsProvider');
        admin = document.getElementById('admin');

        if (nit.value === '' && establecimiento.value === '' && contacto.value === '' && ubicacion.value === '' && direccion.value === '' && descripcion.value === ''
            && capacidad.value === '' && urlweb.value === '' && puntospueblo.value === '' && admin === '') {
            this.growl.show({ life: 10000, severity: 'warn', summary: 'atención', detail: 'el formulario de registro esta vacio, verifique' });
        } else {
            this.providerFood.save(this.state.alimentacion).then(data => {
                this.setState({
                    visibleCard: false,
                    alimentacion: {
                        nit: null,
                        nombreEstablecimiento: null,
                        contacto: null,
                        ubicacion: null,
                        direccion: null,
                        descripcion: null,
                        capacidadAtencion: null,
                        urlWeb: null,
                        puntospueblo: null,
                        admin: {
                            nickName: null
                        }
                    }
                });
                this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'El proveedor de alimentos fue registrado exitosamente' });
                this.providerFood.getAllFood().then(data => this.setState({ providerFood: data }))
            })
        }
    }
    delete() {
        if (window.confirm("Esta a punto de eliminar el proveedor, ¿Esta seguro?")) {
            this.providerFood.delete(this.state.selectedProviderFood.id).then(data => {
                this.growl.show({ severity: 'success', summary: 'Eliminado', detail: 'El proveedor de alimentos fue elimiando exitosamente' });
                this.providerFood.getAllFood().then(data => this.setState({ providerFood: data }));
            });
        }
    }
    render() {
        return (
            <>
                <HeaderUser />
                <div>
                    <Container className="mt-5" fluid>
                        <TabView activeIndex={this.state.activeIndex} onTabChange={(e) => this.setState({ activeIndex: e.index })}>
                            <TabPanel className="tab_item" header="Alimentación">
                                <div className="row">
                                    <div className="col-8">
                                        <div className="box p-3">
                                            <span className="text-title-table-provider"><i className="fas fa-cocktail mr-2"></i>appuebliar | alimentación</span>
                                        </div>
                                        <div className="box-menu-options">
                                            <span className="pi pi-cog mr-2"></span>
                                            <span className="label-actions">Acciones</span>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="card-menu-food">
                                            <div className="row">
                                                <div className="col-8">
                                                    <span className="label-title-menu">Catalogo alimentos</span>
                                                    <div className="position-box-one">
                                                        <MenuFood />
                                                    </div>
                                                </div>
                                                <div className="col-4">
                                                    <img className="img-logo-table" src={require("assets/img/logouno.png")} alt="Logo-appuebliar" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Menubar className="text-menu-actions" model={this.items} />
                                <DataTable value={this.state.providerFood} paginator={true} rows={10} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedProviderFood} onSelectionChange={e => this.setState({ selectedProviderFood: e.value })}>
                                    <Column field="id" header="N." sortable={true} style={{ width: '5%' }}></Column>
                                    <Column field="nit" header="NIT"  style={{ width: '10%' }}  filter={true} filterPlaceholder="Buscar por NIT"></Column>
                                    <Column field="nombreEstablecimiento" header="Establecimiento"  filter={true} filterPlaceholder="Buscar por nombre"></Column>
                                    <Column field="contacto" header="Telefono" style={{ width: '9%' }}></Column>
                                    <Column field="ubicacion" header="Ciudad"  filter={true} filterPlaceholder="Buscar por ciudad"></Column>
                                    <Column field="direccion" header="Dirección"></Column>
                                    <Column field="capacidadAtencion" header="Capacidad" style={{ width: '7%' }} filter={true} filterPlaceholder="Filtrar"></Column>
                                    <Column field="urlWeb" header="Pagina web" responsive={true} ></Column>
                                    <Column field="puntospueblo" header="Puntos" style={{ width: '6%' }}></Column>
                                </DataTable>
                            </TabPanel>
                            <TabPanel header="Transporte">
                                <FormInputProvider />
                            </TabPanel>
                            <TabPanel header="Alojamiento">
                                <FormInputProviderHotel />
                            </TabPanel>
                            <TabPanel header="Actividades">
                                <FormInputProviderActivity />
                            </TabPanel>
                        </TabView>
                    </Container>
                    <Dialog header="Registrar proveedor | alimentación" visible={this.state.visibleCard} style={{ width: '60%' }} modal={true} onHide={() => this.setState({ visibleCard: false })} footer={this.footer}>
                        <form id="transporte-form">
                            <div className="container-fluid mt-3 mb-3">
                                <div className="box mb-3">
                                    <span className="label-title-form-register"><i className="fas fa-book mr-2"></i>Formulario de registro</span>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="row mt-3">
                                            <div className="col-6">
                                                <label className="label-form">NIT (Registro ante C.C.)</label>
                                                <input className="input-form" type="text" id="nit" value={this.state.alimentacion.nit} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.nit = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Nombre del establecimiento</label>
                                                <input className="input-form" type="text" id="nameProvider" value={this.state.alimentacion.nombreEstablecimiento} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.nombreEstablecimiento = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Telefono</label>
                                                <input className="input-form" type="text" id="phoneProvider" value={this.state.alimentacion.contacto} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.contacto = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Dirección</label>
                                                <input type="text" id="addressProvider" className="input-form" value={this.state.alimentacion.direccion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.direccion = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Ciudad donde esta ubicado</label>
                                                <input type="text" className="input-form" id="cityProvider" value={this.state.alimentacion.ubicacion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.ubicacion = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Puntos pueblo</label>
                                                <input className="input-form" type="text" id="pointsProvider" value={this.state.alimentacion.puntospueblo} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.puntospueblo = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Capacidad de atención</label>
                                                <input className="input-form" type="text" id="capacidad" value={this.state.alimentacion.capacidadAtencion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.capacidadAtencion = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Url WEB</label>
                                                <input className="input-form" type="text" id="urlweb" value={this.state.alimentacion.urlWeb} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.urlWeb = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">¿Quien registra? - Nickname</label>
                                                <input className="input-form" type="text" id="admin" value={this.state.alimentacion.admin.nickName} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.admin.nickName = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-12">
                                                <label className="label-form">Agregar información adicional sobre el establecimiento</label>
                                                <InputTextarea className="input-form" style={{ width: '100%' }} rows={5} cols={30} autoResize={true} id="description" value={this.state.alimentacion.descripcion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alimentacion = Object.assign({}, prevState.alimentacion);
                                                        alimentacion.descripcion = val;
                                                        return { alimentacion };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Dialog>
                </div>
                <script src="/src/assets/js/tabpanel.js"></script>
                <Growl ref={(el) => this.growl = el} />
            </>
        );
    }
    cleanForm() {
        document.getElementById('transporte-form').reset();
    }
    showEditDialog() {
        this.setState({
            visibleCard: true,
            alimentacion: {
                nit: this.state.selectedProviderFood.nit,
                nombreEstablecimiento: this.state.selectedProviderFood.nombreEstablecimiento,
                contacto: this.state.selectedProviderFood.contacto,
                ubicacion: this.state.selectedProviderFood.ubicacion,
                direccion: this.state.selectedProviderFood.direccion,
                descripcion: this.state.selectedProviderFood.descripcion,
                capacidadAtencion: this.state.selectedProviderFood.capacidadAtencion,
                urlWeb: this.state.selectedProviderFood.urlWeb,
                puntospueblo: this.state.selectedProviderFood.puntospueblo,
                admin: {
                    nickName: this.state.selectedProviderFood.nickName
                }
            }
        })
    }
}
export default proveedores;