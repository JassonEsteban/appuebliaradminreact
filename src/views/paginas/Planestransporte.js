/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import CabezotePlanes from "components/header/Headerplanes";
import axios from "axios";
export default class Planestransporte extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const { values, handleChange } = this.props;
        return (
            <div>
                <CabezotePlanes />
                <section className="container">
                    <div className="card-transport-plan">
                        <div className="card-header-date">
                            <label className="label-title-name-plan">Transporte</label>
                        </div>
                        <div className="body-card">
                            <div className="row">
                                <div className="col-6">
                                    <img className="img-banner-plan" src={require("assets/img/2791967.jpg")} alt="banner imagen transporte" />
                                </div>
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-title-item">
                                                <label className="label-item">Agregar Transportadora</label>
                                            </div>
                                            <div className="box-input-plan">
                                                <label className="label-form-item">Ingrese el codigo</label>
                                                <input className="input-form-plan" type="text" id="codigoProviderTransport" name="codigoTransporte" onChange={handleChange('codigoTransporte')} defaultValue={values.codigoTransporte} placeholder="Codigo del transportador"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="box-btn-validate">
                                                <button type="button" className="btn-validate-item" onClick={this.validateProvider}>Agregar</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="container">
                                        <div className="card-result-item" id="cardResult">
                                            <div className="row">
                                                <div className="col-4">
                                                    <img className="img-banner-plan" src={require("assets/img/autobus.png")} alt="error" />
                                                </div>
                                                <div className="col-8">
                                                    <div className="header-card-result">
                                                        <label className="label-title-result">Transportadora <label className="label-result-item-codex" id="codigo" onChange={handleChange('codigoTransporte')} defaultValue={values.codigoTransporte}></label> </label>
                                                    </div>
                                                    <div className="body-card-result">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="box-name-provider">
                                                                    <label className="label-title-provider" id="nombre"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="direccion" className="label-result-item" ></label>
                                                                </div>
                                                            </div>
                                                            <div className="col-4">
                                                                <div className="box-item-provider">
                                                                    <label id="contacto" className="label-result-item" ></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-8">
                                                                <div className="box-item-provider">
                                                                    <label id="ubicacion" className="label-city-provider"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-result-item-error" id="carderror">
                                            <div className="body-card-result">
                                                <div className="row">
                                                    <div className="col-4">
                                                        <img className="img-banner-plan" src={require("assets/img/error.png")} alt="error" />
                                                    </div>
                                                    <div className="col-8">
                                                        <div className="box-error">
                                                            <label id="errormsg" className="label-error-msg" id="labelerror"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>





                                </div>
                            </div>
                            <div className="card-detail-item">
                                <div className="box-title-item-provider">
                                    <h1 className="title-item-provider-food">Agregar detalles</h1>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Tipo de vehiculo</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('tipoVehiculo')} defaultValue={values.tipoVehiculo} id="tipoVehiculo">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="Bus de 34 puestos">Bus (34 puestos)</option>
                                                <option value="Buseta de 20 puestos">Buseta (20 puestos)</option>
                                                <option value="MicroBus de 15 puestos">MicroBus (15 puestos)</option>
                                                <option value="Camioneta">Vehiculo mediano (Camioneta)</option>
                                                <option value="Sedan 4 puestos">Vehiculo pequeño (Sedan)</option>
                                                <option value="Van 8 puestos">Van (8 puestos)</option>
                                                <option value="Vehiculo premium">Vehiculo premium</option>
                                                <option value="Vehiculo especial">Otro vehiculo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">Precio del pasaje</label>
                                            <input className="input-form-plan" type="text" onChange={handleChange('precioPasaje')} defaultValue={values.precioPasaje} placeholder="Ingrese el valor"></input>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye otro conductor?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('otroConductor')} defaultValue={values.otroConductor} id="otroconductor">
                                                <option value="todo incluido">Seleccione una opcion</option>
                                                <option value="conductor auxiliar">SI</option>
                                                <option value="Conductor auxiliar no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye wifi a bordo?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('wifiAbordo')} defaultValue={values.wifiAbordo} id="wifiAbordo">
                                                <option value="todo incluido">Seleccione una opcion</option>
                                                <option value="wifi incluido">SI</option>
                                                <option value="WIfi no incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Incluye baño (WC)?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('wcAbordo')} defaultValue={values.wcAbordo} id="wcAbordo">
                                                <option value="todo incluido">Seleccione una opcion</option>
                                                <option value="Baño (WC) incluido">SI</option>
                                                <option value="Baño (WC) No incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className="box-input-plan">
                                            <label className="label-form-plan">¿Paradas en ruta?</label>
                                            <select name="select" className="select-type-plan" onChange={handleChange('paradas')} defaultValue={values.paradas} id="paradas">
                                                <option value="">Seleccione una opcion</option>
                                                <option value="Paradas en ruta incluidas">SI</option>
                                                <option value="Paradas en ruta No incluido">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="box-input-plan-description">
                                            <label className="label-form-plan-description">Informacion adicional</label>
                                            <textarea name="textarea" className="input-form-plan-description" rows="3" cols="50" onChange={handleChange('descripcionVehiculo')} defaultValue={values.descripcionVehiculo} placeholder="Descripcion breve del vehiculo, rutas, itinerario..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="footer-card">
                                <div className="box-btn-save-data">
                                    <button type="button" className="btn-save-information-back" onClick={this.back}>Regresar</button>
                                    <button type="button" className="btn-save-information" onClick={this.continue}>Guardar y continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        );
    }
   
    validateProvider() {
        var urlProvider = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/proveedortransporte/";
        var codexProviderFood;
        codexProviderFood = document.getElementById('codigoProviderTransport');
        console.log(codexProviderFood.value);
        axios.get(urlProvider + codexProviderFood.value).then(function (response) {
            console.log(response);
            console.log(response.data);
            if (response.data.response === "BLANK") {
                var box = document.getElementById('carderror');
                box.style.display = "block";
                var div = document.getElementById('cardResult');
                div.style.display = "none";
                document.getElementById('labelerror').innerHTML = "Lo sentimos, este proveedor no existe :(";
            } else {
                var div = document.getElementById('cardResult');
                div.style.display = "block";
                var box = document.getElementById('carderror');
                box.style.display = "none";
                document.getElementById('codigo').innerHTML = response.data.id;
                document.getElementById('nombre').innerHTML = response.data.nombre;
                document.getElementById('ubicacion').innerHTML = response.data.ubicacion;
                document.getElementById('direccion').innerHTML = response.data.direccion;
                document.getElementById('contacto').innerHTML = response.data.contacto;
            }
        })
    }
}