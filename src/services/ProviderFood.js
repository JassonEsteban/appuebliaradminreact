import axios from "axios";

 class ProviderFood {
    baseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/proveedoralimentacion";

    getAllFood(){
        return axios.get(this.baseUrl).then(res => res.data);
    }
    
    save(alimentacion){
        return axios.put(this.baseUrl, alimentacion).then(res => res.data);
    }

    delete(id){
        return axios.delete(this.baseUrl +"/" +id).then(res => res.data);
    }
}

export default ProviderFood;