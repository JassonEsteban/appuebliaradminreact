import axios from "axios";

class validateAdmin {
    baseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/admin/correo";
    Url = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/admin";

    getAdmin(correo){
        return axios.get(this.baseUrl + "/" + correo).then(res => res.data);
    }

    getAdmins(){
        return axios.get(this.Url).then(res => res.data);
    }

}

export default validateAdmin;