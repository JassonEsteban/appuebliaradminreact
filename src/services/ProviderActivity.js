import axios from "axios";

 class ProviderActivity {
    baseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/actividad";
    getAllActivity(){
        return axios.get(this.baseUrl).then(res => res.data);
    }

    save(actividad){
        return axios.put(this.baseUrl, actividad).then(res => res.data);
    }

    delete(id){
        return axios.get(this.baseUrl+"/"+ id).then(res => res.data);
    }
}

export default ProviderActivity;