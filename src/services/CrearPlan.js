import axios from "axios";

class CrearPlan {
    baseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/plan";
    crearviaje(plan){
        return axios.put(this.baseUrl, plan).then(res => 
            res.data
            );
    }
}

export default CrearPlan;