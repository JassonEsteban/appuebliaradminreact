import axios from "axios";

 class ProviderHotel {
    baseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/proveedoralojamiento";
    getAllHotel(){
        return axios.get(this.baseUrl).then(res => res.data);
    }

    save(transporte){
        return axios.put(this.baseUrl, transporte).then(res => res.data);
    }

    delete(id){
        return axios.get(this.baseUrl +"/" +id).then(res => res.data);
    }
}

export default ProviderHotel;