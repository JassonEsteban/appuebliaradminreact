import axios from "axios";

class CrearSalida {
    baseUrl = "http://ec2-3-137-182-178.us-east-2.compute.amazonaws.com/salida";
    createExit(exit){
        return axios.put(this.baseUrl, exit).then(res => 
            res.data
            );
    }
}

export default CrearSalida;