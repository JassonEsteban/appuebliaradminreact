import axios from "axios";

class servicesAddFood {
    Url = "http://localhost:2000/tipo_alimentacion";

    getAllMenu(){
        return axios.get(this.Url).then(res => res.data);
    }
     
    savefood(food){
        return axios.put(this.Url, food).then(res => res.data);
    }

    delete(codigo){
        return axios.get(this.Url +"/" + codigo).then(res => res.data);
    }


}

export default servicesAddFood;