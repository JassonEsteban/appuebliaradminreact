import axios from "axios";

class servicesAddCar {
    Url = "http://localhost:2000/tipo_transporte";

    getAllCars(){
        return axios.get(this.Url).then(res => res.data);
    }
     
    savecar(car){
        return axios.put(this.Url, car).then(res => res.data);
    }

    delete(codigo){
        return axios.get(this.Url +"/" + codigo).then(res => res.data);
    }


}

export default servicesAddCar;