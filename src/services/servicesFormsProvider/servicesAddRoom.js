import axios from "axios";

class servicesAddRoom {
    Url = "http://localhost:2000/tipo_habitacion";

    getAllRooms(){
        return axios.get(this.Url).then(res => res.data);
    }
     
    saveroom(car){
        return axios.put(this.Url, car).then(res => res.data);
    }

    delete(codigo){
        return axios.get(this.Url +"/" + codigo).then(res => res.data);
    }


}

export default servicesAddRoom;