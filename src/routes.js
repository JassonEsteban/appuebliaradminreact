import Index from "views/index.js";
import Planes from "views/paginas/Planes.js";
import proveedores from "views/paginas/proveedores.js";
import login from "views/paginas/login";
import "primeicons/primeicons.css";

var routes = [
    {
        path: "/index",
        name: "Inicio",
        Icon: "fas fa-beer",
        component: Index,
        layout: "/admin"
    },
    {
        path: "/proveedores",
        name: "Proveedores",
        Icon: "directions_walk",
        component: proveedores,
        layout: "/admin"
    },
    {
        path: "/planes",
        name: "Crear plan",
        Icon: "fas fa-leaf text-blue",
        component: Planes,
        layout: "/admin"
    },
    {
        path: "/login",
        name: "",
        Icon: "",
        component: login,
        layout: "/auth"
    }
];

export default routes;