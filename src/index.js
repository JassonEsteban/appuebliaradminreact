import React from 'react';
import ReactDom from "react-dom";
import { BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import "./assets/css/appuebliar-react.css";
import "./assets/css/appuebliar.min.css";
import "./assets/css/all.min.css";
import "./assets/css/all.css";
import "./assets/css/fontawesome.css";
import "./assets/css/fontawesome.min.css";
import "./assets/plugins/nucleo/css/nucleo.css";
import "./assets/css/styles.scss";
import AdminLayout from "./layout/Admin.js";
import AuthLayout from "./layout/usuario.js";

ReactDom.render(
  <BrowserRouter>
  <Switch>
    <Route path="/admin" render={props => <AdminLayout {...props} />} />
    <Route path="/auth" render={ props => <AuthLayout {...props}/>}/>
    <Redirect from="/" to="/auth/login" />
  </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);