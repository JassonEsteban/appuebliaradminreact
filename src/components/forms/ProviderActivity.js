import React from "react";
import Provideractivity from "services/ProviderActivity.js";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { Growl } from 'primereact/growl';
import { Menubar } from 'primereact/menubar';
import "primereact/resources/themes/nova-dark/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";

class ProviderActivity extends React.Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            actividad: {
                codigo: null,
                nombreActividad: null,
                fechaInicioActividad: null,
                fechaFinActividad: null,
                ubicacion: null,
                operador: null,
                precioPersona: null,
                horaInicio: null,
                descripcion: null,
                capacidadPersonas: null,
                lugarActividad: null,
                contacto: null,
                puntospueblo: null
            },
            selectedProviderActividad: {
            }
        };
        this.es = {
            firstDayOfWeek: 1,
            dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
            dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: 'Hoy',
            clear: 'Limpiar',
            dateFormat: 'dd/mm/yy',
            weekHeader: 'Sm'
        };
        this.items = [
            {
                label: 'Registrar',
                icon: 'pi pi-fw pi-plus',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Editar',
                icon: 'pi pi-fw pi-pencil',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Eliminar',
                icon: 'pi pi-fw pi-trash',
                command: () => { this.delete() }
            },
            {
                label: 'Ver mas',
                icon: 'pi pi-fw pi-eye',
                command: () => { this.delete() }
            }
        ];
        this.ProviderActivity = new Provideractivity();
        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.footer = (
            <div className="box-footer">
                <button className="button-add-provider" onClick={this.save} >Registrar actividad</button>
            </div>
        );
    }
    componentDidMount() {
        this.ProviderActivity.getAllActivity().then(data => this.setState({ ProviderActivity: data }))
    }
    save() {
        this.ProviderActivity.save(this.state.actividad).then(data => {
            this.setState({
                visible: false,
                actividad: {
                    codigo: null,
                    nombreActividad: null,
                    fechaInicioActividad: null,
                    fechaFinActividad: null,
                    ubicacion: null,
                    operador: null,
                    precioPersona: null,
                    horaInicio: null,
                    descripcion: null,
                    capacidadPersonas: null,
                    lugarActividad: null,
                    contacto: null,
                    puntospueblo: null
                }
            })
            this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'La actividad fue registrada exitosamente' });
            this.ProviderActivity.getAllActivity().then(data => this.setState({ ProviderActivity: data }))
        }) 
    }
    delete() {
        if (window.confirm("Esta a punto de eliminar esta actividad, ¿Esta seguro?")) {
            this.ProviderActivity.delete(this.state.selectedProviderActividad.codigo).then(data => {
                this.growl.show({ severity: 'success', summary: 'Eliminado', detail: 'La actividad fue elimianda exitosamente' });
                this.ProviderActivity.getAllActivity().then(data => this.setState({ ProviderActivity: data }));
            });
        }
    }

    
    render() {
        return (
            <>
                <div>
                    <div className="row">
                        <div className="col-10">
                            <div className="box p-3">
                                <span className="text-dark text-title-table-provider"><i className="fas fa-bicycle mr-2"></i>Proveedores de Actividades</span>
                            </div>
                            <div className="box-menu-options">
                                <span className="pi pi-cog mr-2"></span>
                                <span className="label-actions">Acciones</span>
                            </div>
                        </div>
                        <div className="col-2">
                            <img className="img-logo-table" src={require("assets/img/logouno.png")} alt="Logo-appuebliar" />
                        </div>
                    </div>
                    <Menubar model={this.items} />
                    <DataTable value={this.state.ProviderActivity} paginator={true} rows={10} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedProviderActividad} onSelectionChange={e => this.setState({ selectedProviderActividad: e.value })}>
                        <Column field="codigo" header="Id" sortable={true} filter={true} filterPlaceholder="Filtrar" style={{ width: '5%' }}></Column>
                        <Column field="nombreActividad" header="Actividad" filter={true} filterPlaceholder="Buscar por nombre"></Column>
                        <Column field="fechaInicioActividad" header="Fecha inicio"></Column>
                        <Column field="fechaFinActividad" header="Fecha fin"></Column>
                        <Column field="ubicacion" header="Ciudad" filter={true} filterPlaceholder="Buscar por ciudad"></Column>
                        <Column field="operador" header="operador" filter={true} filterPlaceholder="Filtrar"></Column>
                        <Column field="precioPersona" header="Precio persona"></Column>
                        <Column field="horaInicio" header="Hora inicio" responsive={true}></Column>
                        <Column field="descripcion" header="Descripcion"></Column>
                        <Column field="capacidadPersonas" header="Capacidad"  filter={true} filterPlaceholder="Filtrar"></Column>
                        <Column field="lugarActividad" header="Dirección"></Column>
                        <Column field="contacto" header="contacto"></Column>
                        <Column field="puntospueblo" header="Puntos" style={{ width: '6%' }}></Column>
                    </DataTable>
                    <Dialog header="Registrar actividad | actividades y entretenimiento" visible={this.state.visible} style={{ width: '50%' }} modal={true} onHide={() => this.setState({ visible: false })} footer={this.footer}>
                        <form id="transporte-form">
                            <div className="container-fluid mt-3 mb-3">
                                <div className="box mb-3">
                                    <span className="label-title-form-register"><i className="fas fa-book mr-2"></i>Formulario de registro</span>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-6">
                                        <label className="label-form">Codigo de la actividad</label>
                                        <input className="input-form" type="text" id="codigo" value={this.state.actividad.codigo} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.codigo = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                    <div className="col-6">
                                        <label className="label-form">Nombre de la actividad</label>
                                        <input className="input-form" type="text" id="nameActivity" value={this.state.actividad.nombreActividad} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.nombreActividad = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="label-form">Fecha de inicio</label>
                                        <input className="input-form" type="date"  id="fechaInicio"  value={this.state.actividad.fechaInicioActividad}
                                            onChange={(e) => {
                                                let val = e.target.value;
                                                this.setState(prevState => {
                                                    let actividad = Object.assign({}, prevState.actividad);
                                                    actividad.fechaInicioActividad = val;
                                                    return { actividad };
                                                })
                                            }}/>
                                    </div>
                                    <div className="col-6">
                                        <label className="label-form">Fecha fin</label>
                                        <input className="input-form" type="date" id="fechaFin" style={{ width: '100%' }} value={this.state.actividad.fechaFinActividad}
                                            onChange={(e) => {
                                                let val = e.target.value;
                                                this.setState(prevState => {
                                                    let actividad = Object.assign({}, prevState.actividad);
                                                    actividad.fechaFinActividad = val;
                                                    return { actividad };
                                                })
                                            }}/>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Ciudad donde se realizará</label>
                                        <input className="input-form" type="text" id="cityActivity" value={this.state.actividad.ubicacion} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.ubicacion = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                    <div className="col-6">
                                        <label className="label-form">Operador o persona encargada</label>
                                        <input className="input-form" type="text" id="operator" value={this.state.actividad.operador} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.operador = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="label-form">Precio por persona</label>
                                        <input className="input-form" type="text" id="pricePeople" value={this.state.actividad.precioPersona} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.precioPersona = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                    <div className="col-6">
                                        <label className="label-form">Hora de inicio</label>
                                        <input className="input-form" type="time" id="horaInicio" style={{ width: '100%' }} value={this.state.actividad.horaInicio}
                                            onChange={(e) => {
                                                let val = e.target.value;
                                                this.setState(prevState => {
                                                    let actividad = Object.assign({}, prevState.actividad);
                                                    actividad.horaInicio = val;
                                                    return { actividad };
                                                })
                                            }}/>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="label-form">Cantidad de personas limite</label>
                                        <input className="input-form" type="text" id="cantPeople" value={this.state.actividad.capacidadPersonas} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.capacidadPersonas = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                    <div className="col-6">
                                        <label className="label-form">Direccion o punto de referencia</label>
                                        <input className="input-form" type="text" id="addressActivity" value={this.state.actividad.lugarActividad} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.lugarActividad = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="label-form">Telefono</label>
                                        <input className="input-form" type="text" id="contacto" value={this.state.actividad.contacto} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.contacto = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                    <div className="col-6">
                                        <label className="label-form">Puntos pueblo</label>
                                        <input className="input-form" type="text" id="pointsTown" value={this.state.actividad.puntospueblo} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.puntospueblo = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-12">
                                        <label className="label-form">Descripción de la actividad</label>
                                        <InputTextarea style={{ width: '100%' }} rows={5} cols={30} autoResize={true} id="description" value={this.state.actividad.descripcion} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let actividad = Object.assign({}, prevState.actividad);
                                                actividad.descripcion = val;
                                                return { actividad };
                                            })
                                        }} />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Dialog>
                </div>
                <Growl ref={(el) => this.growl = el} />
            </>
        );
    }

    showEditDialog() {
        this.setState({
            visible: true,
            actividad: {
                codigo: this.state.selectedProviderActividad.codigo,
                nombreActividad: this.state.selectedProviderActividad.nombreActividad,
                fechaInicioActividad: this.state.selectedProviderActividad.fechaInicioActividad,
                fechaFinActividad: this.state.selectedProviderActividad.fechaFinActividad,
                ubicacion: this.state.selectedProviderActividad.ubicacion,
                operador: this.state.selectedProviderActividad.operador,
                precioPersona: this.state.selectedProviderActividad.precioPersona,
                horaInicio: this.state.selectedProviderActividad.horaInicio,
                descripcion: this.state.selectedProviderActividad.descripcion,
                capacidadPersonas: this.state.selectedProviderActividad.capacidadPersonas,
                lugarActividad: this.state.selectedProviderActividad.lugarActividad,
                contacto: this.state.selectedProviderActividad.contacto,
                puntospueblo: this.state.selectedProviderActividad.puntospueblo,
            }
        })
    }


}

export default ProviderActivity;