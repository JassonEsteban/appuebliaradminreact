import React from "react";
import Providerhotel from "services/ProviderHotel.js";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dialog } from 'primereact/dialog';
import { Growl } from 'primereact/growl';
import { Menubar } from 'primereact/menubar';
import MenuRoom from "components/InsertForms/MenuRoom.js";
import "primereact/resources/themes/nova-dark/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
class ProviderHotel extends React.Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            alojamiento: {
                nit: null,
                nombre: null,
                contacto: null,
                ubicacion: null,
                direccion: null,
                urlweb: null,
                puntospueblo: null,
                admin: {
                    nickName: null
                }
            },
            selectedProvideralojamiento: {
            }
        };

        this.es = {
            firstDayOfWeek: 1,
            dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
            dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: 'Hoy',
            clear: 'Limpiar',
            dateFormat: 'dd/mm/yy',
            weekHeader: 'Sm'
        };

        this.items = [
            {
                label: 'Registrar',
                icon: 'pi pi-fw pi-plus',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Editar',
                icon: 'pi pi-fw pi-pencil',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Eliminar',
                icon: 'pi pi-fw pi-trash',
                command: () => { this.delete() }
            },
            {
                label: 'ver mas',
                icon: 'pi pi-fw pi-eye',
                command: () => { this.delete() }
            }
        ];
        this.Provideralojamiento = new Providerhotel();
        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.footer = (
            <div className="box-footer">
                <button className="button-add-provider" onClick={this.save} >Agregar proveedor</button>
            </div>
        );
    }
    componentDidMount() {
        this.Provideralojamiento.getAllHotel().then(data => this.setState({ Provideralojamiento: data }))
    }
    save() {
        this.Provideralojamiento.save(this.state.alojamiento).then(data => {
            this.setState({
                visible: false,
                alojamiento: {
                    nit: null,
                    nombre: null,
                    contacto: null,
                    ubicacion: null,
                    direccion: null,
                    urlweb: null,
                    puntospueblo: null,
                    admin: {
                        nickName: null
                    }
                }
            });
            this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'El proveedor de hospedaje fue registrado exitosamente' });
            this.Provideralojamiento.getAllHotel().then(data => this.setState({ Provideralojamiento: data }))
        })
    }

    delete() {
        if (window.confirm("Esta a punto de eliminar el proveedor, ¿Esta seguro?")) {
            this.Provideralojamiento.delete(this.state.selectedProvideralojamiento.id).then(data => {
                this.growl.show({ severity: 'success', summary: 'Eliminado', detail: 'El proveedor de hospedaje fue elimiando exitosamente' });
                this.Provideralojamiento.getAllHotel().then(data => this.setState({ Provideralojamiento: data }));
            });
        }
    }
    render() {
        return (
            <>
                <div>
                    <div className="row">
                        <div className="col-8">
                            <div className="box p-3">
                                <span className="text-title-table-provider"><i className="fas fa-hotel mr-2"></i>appuebliar | alojamientos</span>
                            </div>
                            <div className="box-menu-options">
                                <span className="pi pi-cog mr-2"></span>
                                <span className="label-actions">Acciones</span>
                            </div>
                        </div>
                        <div className="col-4">
                            <div className="card-menu-food">
                                <div className="row">
                                    <div className="col-8">
                                        <span className="label-title-menu">Catalogo habitaciones</span>
                                        <div className="position-box-one">
                                            <MenuRoom />
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <img className="img-logo-table" src={require("assets/img/logouno.png")} alt="Logo-appuebliar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Menubar model={this.items} />
                    <DataTable value={this.state.Provideralojamiento} paginator={true} rows={10} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedProvideralojamiento} onSelectionChange={e => this.setState({ selectedProvideralojamiento: e.value })}>
                        <Column field="codigo" header="ID" sortable={true} style={{ width: '5%' }}></Column>
                        <Column field="nit" header="NIT"  filter={true} filterPlaceholder="Buscar por NIT" style={{ width: '11%' }}></Column>
                        <Column field="nombre" header="Nombre" filter={true} filterPlaceholder="Buscar por nombre" style={{ width: '18%' }}></Column>
                        <Column field="contacto" header="Contacto" style={{ width: '7%' }}></Column>
                        <Column field="ubicacion" header="Ubicacion" filter={true} filterPlaceholder="Buscar por ciudad" style={{ width: '15%' }}></Column>
                        <Column field="direccion" header="Direccion" style={{ width: '15%' }}></Column>
                        <Column field="urlweb" header="Pagina web"></Column>
                        <Column field="puntospueblo" header="Puntos" style={{ width: '6%' }}></Column>
                    </DataTable>
                    <Dialog header="Registrar proveedor | alojamientos" visible={this.state.visible} style={{ width: '50%' }} modal={true} onHide={() => this.setState({ visible: false })} footer={this.footer}>
                        <form id="transporte-form">
                            <div className="container-fluid mt-3 mb-3">
                                <div className="box mb-3">
                                    <span className="label-title-form-register"><i className="fas fa-book mr-2"></i>Formulario de registro</span>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="row mt-3">
                                            <div className="col-6">
                                                <label className="label-form">NIT (Registro ante C. C.)</label>
                                                <input className="input-form" type="text" id="nit" value={this.state.alojamiento.nit} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.nit = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Nombre del alojamiento</label>
                                                <input className="input-form" type="text" id="nameProvider" value={this.state.alojamiento.nombre} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.nombre = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Telefono</label>
                                                <input className="input-form" type="text" id="phoneProvider" value={this.state.alojamiento.contacto} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.contacto = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Dirección</label>
                                                <input className="input-form" type="text" id="addressProvider" value={this.state.alojamiento.direccion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.direccion = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Ciudad donde esta ubicado</label>
                                                <input className="input-form" type="text" id="cityProvider" value={this.state.alojamiento.ubicacion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.ubicacion = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Puntos pueblo</label>
                                                <input className="input-form" type="text" id="pointsProvider" value={this.state.alojamiento.puntospueblo} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.puntospueblo = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">¿Quien registra? - Nickname</label>
                                                <input className="input-form" type="text" id="admin" value={this.state.alojamiento.admin.nickName} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.admin.nickName = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Url web</label>
                                                <input className="input-form" type="text" id="urlweb" value={this.state.alojamiento.urlweb} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let alojamiento = Object.assign({}, prevState.alojamiento);
                                                        alojamiento.urlweb = val;
                                                        return { alojamiento };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </form>
                    </Dialog>
                </div>
                <Growl ref={(el) => this.growl = el} />
            </>
        );
    }

    showEditDialog() {
        this.setState({
            visible: true,
            alojamiento: {
                nit: this.state.selectedProvideralojamiento.nit,
                nombre: this.state.selectedProvideralojamiento.nombre,
                contacto: this.state.selectedProvideralojamiento.contacto,
                ubicacion: this.state.selectedProvideralojamiento.ubicacion,
                direccion: this.state.selectedProvideralojamiento.direccion,
                urlweb: this.state.selectedProvideralojamiento.urlweb,
                puntospueblo: this.state.selectedProvideralojamiento.puntospueblo,
                admin: {
                    nickName: null
                }
            }
        })
    }


}

export default ProviderHotel;