import React, {useState, useEffect} from "react";


const fetchFood =() => 
fetch("http://localhost:2000/proveedoralimentacion");

const Food = () => {
    const [info, setInfo] = useState({data: null, loading: null, error: null});

    useEffect(() => {
        setInfo(prevInfo => ({...prevInfo, loading: true}));
        fetchFood()
        .then(res =>res.json())
        .then(res  => {
            setInfo(prevInfo => ({...prevInfo, loading: false, data: res}));
        })
        .catch(err => 
            setInfo(prevState => ({...prevState, loading: false, error: err }))
            );

    }, []);

    return(
        <main>
            {info.loading && <p>cargando....</p>}
            {info.error && <p>UPS....tenemos fallas tecnicas</p>}
            <h1>Bienvenido appuebliar</h1>
            <fetchFood.nit></fetchFood.nit>
        </main>
    );
};

export default Food;