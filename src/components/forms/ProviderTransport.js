import React from "react";
import Providertransporte from "services/ProviderTransport.js";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { Growl } from 'primereact/growl';
import { Menubar } from 'primereact/menubar';
import MenuCar from "components/InsertForms/MenuCar.js";
import "primereact/resources/themes/nova-dark/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";

class forminputprovider extends React.Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            transporte: {
                nit: null,
                nombre: null,
                contacto: null,
                ubicacion: null,
                direccion: null,
                descripcion: null,
                puntospueblo: null,
                admin: {
                    nickName: null
                }
            },
            selectedProviderTransporte: {

            }
        };

        this.items = [
            {
                label: 'Registrar',
                icon: 'pi pi-fw pi-plus',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Editar',
                icon: 'pi pi-fw pi-pencil',
                command: () => { this.showEditDialog() }
            },
            {
                label: 'Eliminar',
                icon: 'pi pi-fw pi-trash',
                command: () => { this.delete() }
            },
            {
                label: 'ver mas',
                icon: 'pi pi-fw pi-eye',
                command: () => { this.delete() }
            }
        ];
        this.Providertransporte = new Providertransporte();
        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.footer = (
            <div className="box-footer">
                <button className="button-add-provider" onClick={this.save} >Agregar proveedor</button>
            </div>
        );
    }

    componentDidMount() {
        this.Providertransporte.getAllTransport().then(data => this.setState({ Providertransporte: data }))
    }

    save() {
        this.Providertransporte.save(this.state.transporte).then(data => {
            this.setState({
                visible: false,
                transporte: {
                    nit: null,
                    nombre: null,
                    contacto: null,
                    ubicacion: null,
                    direccion: null,
                    descripcion: null,
                    puntospueblo: null,
                    admin: {
                        nickName: null
                    }
                }
            });
            this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'El proveedor de transporte fue registrado exitosamente' });
            this.Providertransporte.getAllTransport().then(data => this.setState({ Providertransporte: data }))
        })
    }

    delete() {
        if (window.confirm("Esta a punto de eliminar el proveedor, ¿Esta seguro?")) {
            this.Providertransporte.delete(this.state.selectedProviderTransport.id).then(data => {
                this.growl.show({ severity: 'success', summary: 'Eliminado', detail: 'El proveedor de transporte fue elimiando exitosamente' });
                this.Providertransporte.getAllTransport().then(data => this.setState({ Providertransporte: data }));
            });
        }
    }
    render() {
        return (
            <>
                <div>
                    <div className="row">
                        <div className="col-8">
                            <div className="box p-3">
                                <span className="text-title-table-provider"><i className="fas fa-bus mr-2"></i>appuebliar | transportes</span>
                            </div>
                            <div className="box-menu-options">
                                <span className="pi pi-cog mr-2"></span>
                                <span className="label-actions">Acciones</span>
                            </div>
                        </div>
                        <div className="col-4">
                            <div className="card-menu-food">
                                <div className="row">
                                    <div className="col-8">
                                        <span className="label-title-menu">Catalogo vehiculos</span>
                                        <div className="position-box-one">
                                            <MenuCar />
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <img className="img-logo-table" src={require("assets/img/logouno.png")} alt="Logo-appuebliar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Menubar model={this.items} />
                    <DataTable value={this.state.Providertransporte} paginator={true} rows={10} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedProviderTransporte} onSelectionChange={e => this.setState({ selectedProviderTransporte: e.value })}>
                        <Column field="id" header="ID" sortable={true} style={{ width: '5%' }}></Column>
                        <Column field="nit" header="NIT" filter={true} filterPlaceholder="Buscar por NIT" style={{ width: '12%' }}></Column>
                        <Column field="nombre" header="Nombre" filter={true} filterPlaceholder="Buscar por nombre" style={{ width: '18%' }}></Column>
                        <Column field="contacto" header="Contacto" style={{ width: '9%' }}></Column>
                        <Column field="ubicacion" header="Ubicacion"  filter={true} filterPlaceholder="Buscar por ciudad"></Column>
                        <Column field="direccion" header="Direccion"></Column>
                        <Column field="puntospueblo" header="Puntos" style={{ width: '7%' }}></Column>
                    </DataTable>
                    <Dialog header="Registrar proveedor | Transporte" visible={this.state.visible} style={{ width: '60%' }} modal={true} onHide={() => this.setState({ visible: false })} footer={this.footer}>
                        <form id="transporte-form">
                            <div className="container-fluid mt-3 mb-3">
                                <div className="box mb-3">
                                    <span className="label-title-form-register"><i className="fas fa-book mr-2"></i>Formulario de registro</span>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="row mt-3">
                                            <div className="col-6">
                                                <label className="label-form">NIT (Registro ante C.C.)</label>
                                                <input className="input-form" type="text" id="nit" value={this.state.transporte.nit} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.nit = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Nombre de la empresa</label>
                                                <input className="input-form" type="text" id="nameProvider" value={this.state.transporte.nombre} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.nombre = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Telefono</label>
                                                <input className="input-form" type="text" id="phoneProvider" value={this.state.transporte.contacto} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.contacto = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Dirección</label>
                                                <input className="input-form" type="text" id="addressProvider" value={this.state.transporte.direccion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.direccion = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">Ciudad donde esta ubicado</label>
                                                <input className="input-form" type="text" id="cityProvider" value={this.state.transporte.ubicacion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.ubicacion = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                            <div className="col-6">
                                                <label className="label-form">Puntos pueblo</label>
                                                <input className="input-form" type="text" id="pointsProvider" value={this.state.transporte.puntospueblo} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.puntospueblo = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <label className="label-form">¿Quien registra? - Nickname</label>
                                                <input className="input-form" type="text" id="admin" value={this.state.transporte.admin.nickName} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.admin.nickName = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-12">
                                                <label className="label-form">Agregar información adicional</label>
                                                <InputTextarea style={{ width: '100%' }} rows={5} cols={30} autoResize={true} id="description" value={this.state.transporte.descripcion} onChange={(e) => {
                                                    let val = e.target.value;
                                                    this.setState(prevState => {
                                                        let transporte = Object.assign({}, prevState.transporte);
                                                        transporte.descripcion = val;
                                                        return { transporte };
                                                    })
                                                }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Dialog>
                </div>
                <Growl ref={(el) => this.growl = el} />
            </>
        );
    }
    showEditDialog() {
        this.setState({
            visible: true,
            transporte: {
                nit: this.state.selectedProviderTransporte.nit,
                nombre: this.state.selectedProviderTransporte.nombre,
                contacto: this.state.selectedProviderTransporte.contacto,
                ubicacion: this.state.selectedProviderTransporte.ubicacion,
                direccion: this.state.selectedProviderTransporte.direccion,
                descripcion: this.state.selectedProviderTransporte.descripcion,
                puntospueblo: this.state.selectedProviderTransporte.puntospueblo,
                admin: {
                    nickName: this.state.selectedProviderTransporte.nickName
                }
            }
        })
    }


}

export default forminputprovider;