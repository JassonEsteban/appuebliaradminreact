import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Col,  FormGroup, Label, Input, Alert } from 'reactstrap';

const Modaldelete = (props) => {
    const {
        className
    } = props;

    const [modal, setModal] = useState(false);
    const [nestedModal, setNestedModal] = useState(false);
    const [closeAll, setCloseAll] = useState(false);

    const toggle = () => setModal(!modal);
    const toggleNested = () => {
        setNestedModal(!nestedModal);
        setCloseAll(false);
    }
    const toggleAll = () => {
        setNestedModal(!nestedModal);
        setCloseAll(true);
    }

    return (
        <div>
            <Button color="danger" onClick={toggle}><i class="fas fa-trash mr-2"></i>Eliminar</Button>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle} className="text-dark font-weight-bold title-card-modal-delete">Eliminar proveedor</ModalHeader>
                <ModalBody>
                    <Alert className="alert-delete-provider">
                        ATENCIÓN, estas a punto de eliminar el siguiente proveedor
                     </Alert>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>NIT</Label>
                        <Col sm={10}>
                            <Input type="email" name="email" id="exampleEmail"  disabled />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Nombre</Label>
                        <Col sm={10}>
                            <Input type="password" name="password" id="examplePassword" disabled/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Ubicación</Label>
                        <Col sm={10}>
                            <Input type="password" name="password" id="examplePassword" disabled />
                        </Col>
                    </FormGroup>
                    <Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
                        <ModalHeader>ATENCIÓN</ModalHeader>
                        <ModalBody>¿Estas seguro de eliminar el proveedor?</ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={toggleNested}>Eliminar</Button>{' '}
                            <Button color="secondary" onClick={toggleAll}>Cancelar</Button>
                        </ModalFooter>
                    </Modal>
                </ModalBody>
                <ModalFooter>
                <Button color="appuebliarcoloruno" onClick={toggleNested}>Eliminar</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default Modaldelete;