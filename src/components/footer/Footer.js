/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Row, Col, Nav, NavItem, NavLink, Container } from "reactstrap";

class Footer extends React.Component {
    render() {
        return (
            <Container fluid>
                <footer className="footer">
                    <Row className="align-items-center justify-content-xl-betweeen">
                        <Col xl="6">
                            <div className="copyright text-center text-xl-left text-darker">
                                (c) 2020 {" "}
                                <a className="font-weight-bold ml-1 footer-admin"
                                    href=""
                                    rel="noopener noreferrer"
                                    target="_blank"> appuebliar | empresas</a>
                            </div>
                        </Col>
                        <Col xl="6">
                            <Nav className="nav-footer justify-content-center justify-content-xl-end">
                                <NavItem>
                                    <NavLink href="" rel="noopener noreferrer" target="_blank">
                                        J-SON</NavLink>
                                </NavItem>

                                <NavItem>
                                    <NavLink href="" rel="noopener noreferrer" target="_blank">Quienes somos</NavLink>
                                </NavItem>

                                <NavItem>
                                    <NavLink href="" rel="noopener noreferrer" target="_blank">Github</NavLink>
                                </NavItem>

                                <NavItem>
                                    <NavLink href="" rel="noopener noreferrer" target="_blank">Soporte</NavLink>
                                </NavItem>
                            </Nav>
                        </Col>
                    </Row>
                </footer>
            </Container>

        );
    }
}

export default Footer;