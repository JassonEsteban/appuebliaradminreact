/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

import { NavItem, NavLink, Nav, Container, Row, Col} from "reactstrap";

class footerUsuario extends React.Component {
    render(){
        return(
            <>
            <footer className="py-4">
                <Container>
                    <Row className="align-items-center justify-content-xl-between">
                        <Col xl="6">
                            <div className="copyright text-center text-xl-left text-black">
                                (c) 2020 {" "}
                                <a className="font-weight-bold ml-1 color-link-json" href=""  target="_blank"  > J-SON</a>
                            </div>
                        </Col>
                        <Col xl="6">
                            <Nav className="nav-footer justify-content-center justify-content-xl-end">
                                <NavItem>
                                    <NavLink href="" target="_blank">
                                        <div className="json-text">
                                           <span>J-SON</span>
                                        </div>
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="" target="_blank">
                                        <span className="link-footer"> ¿Quienes somos?</span>
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="" target="_blank">
                                    <span className="link-footer">Github</span>
                                    
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="" target="_blank">
                                    <span className="link-footer">Soporte tecnico</span>
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </Col>
                    </Row>
                </Container>
            </footer>
            </>
        );
    }
}

export default footerUsuario;