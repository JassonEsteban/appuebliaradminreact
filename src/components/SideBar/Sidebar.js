/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { NavLink as NavLinkRRD, Link } from "react-router-dom";


import { Collapse, DropdownMenu, DropdownItem, Nav, UncontrolledDropdown, DropdownToggle, Container, Row, NavItem, NavLink, Navbar, NavbarBrand, Media } from "reactstrap";

class Sidebar extends React.Component {
    state = {
        collapseOpen: false
    };

    constructor(props) {
        super(props);
        this.activeRoute.bind(this);
    }

    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    toggleCollapse = () => {
        this.setState({
            collapseOpen: !this.state.collapseOpen
        });
    };

    closeCollapse = () => {
        this.setState({
            collapseOpen: false
        });
    };

    createLinks = routes => {
        return routes.map((prop, key) => {
            return (
                <NavItem key={key}>
                    <NavLink to={prop.layout + prop.path}
                        tag={NavLinkRRD}
                        onClick={this.closeCollapse}
                        activeClassName="active">
                        <i className={prop.icon} /> {prop.name}
                    </NavLink>
                </NavItem>
            );
        });
    };

    render() {
        const { routes, logo } = this.props;
        let navbarBrandProps;
        if (logo && logo.innerLink) {
            navbarBrandProps = {
                to: logo.innerLink,
                tag: Link
            };
        } else if (logo && logo.outterLink) {
            navbarBrandProps = {
                to: logo.outterLink,
                tag: "_blank"
            };
        }
        return (
            <Navbar
                className=" navbar-vertical fixed-left navbar-light bd-white"
                expand="md"
                id="sidenav-main">
                <Container fluid>
                    {/*toggler*/}
                    <button className="navbar-toggler"
                        type="button"
                        onClick={this.toggleCollapse}>
                        <span className="navbar-toggler-icon" />
                    </button>
                    {/*brand */}
                    {logo ? (<NavbarBrand className="pt-0" {...navbarBrandProps}>
                        <img alt={logo.imgAlt}
                            className="navbar-brand-img"
                            src={logo.imgSrc} />
                        <div className="logo-name-program">
                            <span className="label-logo">appuebliar</span>
                        </div>
                    </NavbarBrand>) : null}

                    {/* user */}

                    <Nav className="align-items-center d-md-none">
                        <UncontrolledDropdown nav>
                            <DropdownToggle nav className="nav-link-icon">
                                <i className="ni ni-bell-55" />
                            </DropdownToggle>
                            <DropdownMenu aria-labelledby="navbar-default_dropdown_1"
                                className="dropdown-menu-arrow" right>
                                <DropdownItem>Action</DropdownItem>
                                <DropdownItem>Otra accion</DropdownItem>
                                <DropdownItem divider />
                                <DropdownItem>Algo mas aqui</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>

                        <UncontrolledDropdown nav>
                            <DropdownToggle nav>
                                <Media className="align-items-center">
                                    <span className="avatar avatar-sm rounded-circle">
                                        <img alt="Imagen"
                                            src={require("assets/img/img12.jpg")} />
                                    </span>
                                </Media>
                            </DropdownToggle>
                            <DropdownMenu className="dropdown-menu-arrow" right>
                                <DropdownItem className="noti-title" header tag="div">
                                    <h6 className="text-overflow m-0">Bienvenido</h6>
                                </DropdownItem>
                                <DropdownItem to="/admin/user-profile" tag={Link}>
                                    <i className="ni ni-settings-gear-65" />
                                    <span>Configuracion</span>
                                </DropdownItem>
                                <DropdownItem to="/admin/user-profile" tag={Link}>
                                    <i className="ni ni-settings-gear-65" />
                                    <span>Actividad</span>
                                </DropdownItem>
                                <DropdownItem to="/admin/user-profile" tag={Link}>
                                    <i className="ni ni-settings-gear-65" />
                                    <span>soporte</span>
                                </DropdownItem>
                                <DropdownItem href="#pablo" onClick={e => e.preventDefault()}>
                                    <i className="ni ni-user-run" />
                                    <span>Cerrar sesion</span>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                    <Collapse navbar isOpen={this.state.collapseOpen}>
                        <div className="navbar-collapse-header d-md-none">
                            <Row>

                            </Row>
                        </div>
                    </Collapse>
                    <div className="cajon-menu">
                        <div className="message-menu">
                            <h3 className="text-menu-title">Menu | CRUDS</h3>
                        </div>
                        <div className="box-items-menu">
                            <Nav navbar>
                                {this.createLinks(routes)}
                            </Nav>
                        </div>
                    </div>
                    <div className="footer-name-1">
                        <h5 className="label-text-mark">Appuebliar | viajes</h5>
                    </div>
                </Container>
            </Navbar>
        )
    }
}

export default Sidebar;

