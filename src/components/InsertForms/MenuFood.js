/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-useless-constructor */
/* eslint-disable no-undef */
/* eslint-disable react/no-direct-mutation-state */
import React from "react";
import ServiceAddFood from 'services/servicesFormsProvider/servicesAddFood.js';
import { Dialog } from 'primereact/dialog';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Menu } from 'primereact/menu';
import { Alert } from 'reactstrap';
import { Growl } from 'primereact/growl';

export default class MenuFood extends React.Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            sidebar: false,
            food: {
                id: '',
                nombrePlato: '',
                descripcion: '',
                precio: ''
            },
            selectedFood: {

            }
        };

        this.items = [
            {
                label: 'OPCIONES',
                items: [{ label: 'Agregar', icon: 'pi pi-fw pi-plus', command: () => { this.showEditDialog() } },
                { label: 'Actualizar', icon: 'pi pi-fw pi-pencil' }]
            },
        ];

        this.serviceAddFood = new ServiceAddFood();
        this.saveFoodMenu = this.saveFoodMenu.bind(this);

        this.footer = (
            <div className="box-footer">
                <button className="button-add-provider" onClick={this.saveFoodMenu} >Agregar item</button>
            </div>
        );
    }

    componentDidMount() {
        this.serviceAddFood.getAllMenu().then(data => this.setState({ serviceAddFood: data }))
    }

    saveFoodMenu() {
        this.serviceAddFood.savefood(this.state.food).then(data => {
            this.setState({
                sidebar: false,
                food: {
                    id: '',
                    nombrePlato: '',
                    descripcion: '',
                    precio: ''
                }
            });
            this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'El plato fue registrado exitosamente' });
            this.serviceAddFood.getAllMenu().then(data => this.setState({ serviceAddFood: data }))
        })
    }

    render() {
        return (
            <div>
                <Dialog header="ALIMENTOS | APPUEBLIAR" visible={this.state.visible} style={{ width: '70%' }} modal={true} onHide={() => this.setState({ visible: false })}>
                    <div className="row">
                        <div className="col-3">
                            <label className="text-title-menu-food"><i className="fas fa-hamburger mr-2"></i>Menú</label>
                        </div>
                        <div className="col-9">
                            <Alert color="info">
                                <span className="h5 text-white"><strong>ATENCIÓN</strong>, los siguientes datos son de uso exclusivo para ser agregados cuando se creen planes turisticos</span>
                            </Alert>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2">
                            <Menu model={this.items} />
                        </div>
                        <div className="col-10">
                            <DataTable value={this.state.serviceAddFood} paginator={true} rows={5} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedFood} onSelectionChange={e => this.setState({ selectedFood: e.value })} >
                                <Column field="id" header="Codigo" sortable={true} style={{ width: '10%' }} filterPlaceholder="Buscar"></Column>
                                <Column field="nombrePlato" header="Plato" sortable={true} filter={true} filterPlaceholder="Buscar"></Column>
                                <Column field="descripcion" header="Contenido" filter={true}></Column>
                                <Column field="precio" header="Precio" sortable={true} filterMatchMode="contains" filterPlaceholder="Precio"></Column>
                            </DataTable>
                        </div>
                    </div>

                    <Dialog header="Agregar al menu" visible={this.state.sidebar} style={{ width: '40%' }} modal={true} onHide={() => this.setState({ sidebar: false })} footer={this.footer}>
                        <form id="transporte-form">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-4">
                                        <div className="box-item">
                                            <label className="label-item-add">Codigo del item</label>
                                            <input type="text" className="input-form-item" id="codigo" value={this.state.food.id} onChange={(e) => {
                                                let val = e.target.value;
                                                this.setState(prevState => {
                                                    let food = Object.assign({}, prevState.food);
                                                    food.id = val;
                                                    return { food };
                                                })
                                            }} />
                                        </div>
                                    </div>
                                    <div className="col-8">
                                        <div className="box-item">
                                            <label className="label-item-add">Nombre del alimento</label>
                                            <input type="text" className="input-form-item" id="nombrePlato" value={this.state.food.nombrePlato} onChange={(e) => {
                                                let val = e.target.value;
                                                this.setState(prevState => {
                                                    let food = Object.assign({}, prevState.food);
                                                    food.nombrePlato = val;
                                                    return { food };
                                                })
                                            }} />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-8">
                                        <div className="box-item">
                                            <label className="label-item-add">Ingredientes </label>
                                            <input type="text" className="input-form-item" id="descripcion" value={this.state.food.descripcion} onChange={(e) => {
                                                let val = e.target.value;
                                                this.setState(prevState => {
                                                    let food = Object.assign({}, prevState.food);
                                                    food.descripcion = val;
                                                    return { food };
                                                })
                                            }} />
                                        </div>
                                    </div>
                                    <div className="col-4">
                                    <div className="box-item">
                                            <label className="label-item-add">Precio del alimento</label>
                                            <input type="text" className="input-form-item" id="precio" value={this.state.food.precio} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let food = Object.assign({}, prevState.food);
                                                food.precio = val;
                                                return { food };
                                            })
                                        }} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Dialog>
                    <Growl ref={(el) => this.growl = el} />
                </Dialog>
                <button className="btn-menu-food" onClick={(e) => this.setState({ visible: true })} >
                    <label className="label-btn-menu-food">Ingresar</label>
                </button>

            </div>
        );

    }

    showEditDialog() {
        this.setState({
            sidebar: true,
            food: {
                id: this.state.selectedFood.id,
                nombrePlato: this.state.selectedFood.nombrePlato,
                descripcion: this.state.selectedFood.descripcion,
                precio: this.state.selectedFood.precio
            }
        })
    }

}




