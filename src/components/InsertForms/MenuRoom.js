import React from "react";
import ServiceAddRoom from 'services/servicesFormsProvider/servicesAddRoom.js';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Menu } from 'primereact/menu';
import { Alert } from 'reactstrap';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import {InputTextarea} from 'primereact/inputtextarea';

export default class MenuRoom extends React.Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            sidebar: false,
            room: {
                codigo: '',
                nombre: '',
                descripcion: '',
                precioNoche: '',
                capacidadHabitacion: ''
            },
            selectedRoom: {

            }
        };

        this.items = [
            {
                label: 'OPCIONES',
                items: [{ label: 'Agregar', icon: 'pi pi-fw pi-plus', command: () => { this.showEditDialog()} },
                { label: 'Actualizar', icon: 'pi pi-fw pi-pencil' }]
            },
        ];

        this.serviceAddRoom = new ServiceAddRoom();
        this.saveRoom = this.saveRoom.bind(this);

        this.footer = (
            <div className="box-footer">
                <Button label="Agregar habitacion" className="bottom-add-provider p-button-success" icon="pi pi-check" onClick={this.saveRoom} />
            </div>
        );
    }

    componentDidMount() {
        this.serviceAddRoom.getAllRooms().then(data => this.setState({ serviceAddRoom: data}))
    }

    saveRoom() {
        this.serviceAddRoom.saveroom(this.state.room).then(data => {
            this.setState({
                sidebar: false,
                room: {
                    codigo: '',
                    nombre: '',
                    descripcion: '',
                    precioNoche: '',
                    capacidadHabitacion: ''
                }
            });
            this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'La habitación fue registrada exitosamente' });
            this.serviceAddRoom.getAllRooms().then(data => this.setState({ serviceAddRoom: data }))
        })
    }

    render() {
        return (
            <div>
                <Dialog  header="HABITACIONES | APPUEBLIAR" visible={this.state.visible} style={{ width: '80%' }} modal={true} onHide={() => this.setState({ visible: false })}>
                    <div className="row">
                        <div className="col-3">
                            <label className="text-title-menu-food"><i className="fas fa-concierge-bell mr-2"></i>Alojamientos</label>
                        </div>
                        <div className="col-9">
                            <Alert color="info">
                                <span className="h5 text-white"><strong>ATENCIÓN</strong>, los siguientes datos son de uso exclusivo para ser agregados cuando se creen planes turisticos</span>
                            </Alert>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2">
                            <Menu model={this.items} />
                        </div>
                        <div className="col-10">
                            <DataTable value={this.state.serviceAddRoom} paginator={true} rows={5} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedRoom} onSelectionChange={e => this.setState({ selectedRoom: e.value })} >
                                <Column field="codigo" header="Codigo" sortable={true} style={{ width: '10%' }} filterPlaceholder="Buscar"></Column>
                                <Column field="nombre" header="Habitación" sortable={true} filter={true} filterPlaceholder="Buscar"></Column>
                                <Column field="descripcion" header="Caracteristicas" sortable={true} filter={true}></Column>
                                <Column field="precioNoche" header="Precio estadia" sortable={true} style={{ width: '15%' }} filterPlaceholder="Precio"></Column>
                                <Column field="capacidadHabitacion" header="Capacidad de personas" sortable={true} style={{ width: '10%' }} filterPlaceholder="Precio"></Column>
                            </DataTable>
                        </div>
                    </div>

                    <Dialog header="Registrar habitación" visible={this.state.sidebar} style={{ width: '50%' }} modal={true} onHide={() => this.setState({ sidebar: false })} footer={this.footer}>
                    <form id="transporte-form">
                            <div className="container-fluid mt-3 mb-3">
                                <div className="row mt-3">
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Codigo</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.room.codigo} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let room = Object.assign({}, prevState.room);
                                                room.codigo = val;
                                                return { room };
                                            })
                                        }}  />
                                    </div>
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Nombre habitacion</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.room.nombre} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let room = Object.assign({}, prevState.room);
                                                room.nombre = val;
                                                return { room };
                                            })
                                        }}  />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Precio por persona | noche</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.room.precioNoche} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let room = Object.assign({}, prevState.room);
                                                room.precioNoche = val;
                                                return { room };
                                            })
                                        }}  />
                                    </div>
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Capacidad</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.room.capacidadHabitacion} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let room = Object.assign({}, prevState.room);
                                                room.capacidadHabitacion = val;
                                                return { room };
                                            })
                                        }}  />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-12">
                                        <label className="text-dark font-weight-bold">Caracteristicas de la habitación</label>
                                        <InputTextarea style={{ width: '100%' }} rows={5} cols={30} autoResize={true} id="codigo" value={this.state.room.descripcion} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let room = Object.assign({}, prevState.room);
                                                room.descripcion = val;
                                                return { room };
                                            })
                                        }} />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Dialog>
                    <Growl ref={(el) => this.growl = el} />
                </Dialog>
                <button className="btn-menu-food" onClick={(e) => this.setState({ visible: true })} >
                    <label className="label-btn-menu-food">Ingresar</label>
                </button>
            </div>
        );
    }
    showEditDialog() {
        this.setState({
            sidebar: true,
            room: {
                codigo: this.state.selectedRoom.codigo,
                nombre: this.state.selectedRoom.nombre,
                descripcion: this.state.selectedRoom.descripcion,
                precioNoche: this.state.selectedRoom.precioNoche,
                capacidadHabitacion: this.state.selectedRoom.capacidadHabitacion
            }
        })
    }
}
