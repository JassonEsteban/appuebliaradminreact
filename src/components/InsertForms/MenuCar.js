import React from "react";
import ServiceAddCar from 'services/servicesFormsProvider/servicesAddCar.js';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Menu } from 'primereact/menu';
import { Alert } from 'reactstrap';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import {InputTextarea} from 'primereact/inputtextarea';

export default class MenuCar extends React.Component {
    constructor() {
        super();
        this.state = {
            visible: false,
            sidebar: false,
            car: {
                codigo: '',
                nombre: '',
                modelo: '',
                añoModelo: '',
                capacidad: '',
                precioPersona: '',
                descripcion: ''
            },
            selectedCar: {

            }
        };

        this.items = [
            {
                label: 'OPCIONES',
                items: [{ label: 'Agregar', icon: 'pi pi-fw pi-plus', command: () => { this.showEditDialog()} },
                { label: 'Actualizar', icon: 'pi pi-fw pi-pencil' }]
            },
        ];

        this.serviceAddCar = new ServiceAddCar();
        this.saveCar = this.saveCar.bind(this);

        this.footer = (
            <div className="box-footer">
                <Button label="Agregar vehiculo" className="bottom-add-provider p-button-success" icon="pi pi-check" onClick={this.saveCar} />
            </div>
        );
    }

    componentDidMount() {
        this.serviceAddCar.getAllCars().then(data => this.setState({ serviceAddCar: data}))
    }

    saveCar() {
        this.serviceAddCar.savecar(this.state.car).then(data => {
            this.setState({
                sidebar: false,
                car: {
                    codigo: '',
                    nombre: '',
                    modelo: '',
                    añoModelo: '',
                    capacidad: '',
                    precioPersona: '',
                    descripcion: ''
                }
            });
            this.growl.show({ severity: 'success', summary: 'Registro exitoso', detail: 'El vehiculo fue registrado exitosamente' });
            this.serviceAddCar.getAllCars().then(data => this.setState({ serviceAddCar: data }))
        })
    }

    render() {
        return (
            <div>
                <Dialog  header="VEHICULOS | APPUEBLIAR" visible={this.state.visible} style={{ width: '80%' }} modal={true} onHide={() => this.setState({ visible: false })}>
                    <div className="row">
                        <div className="col-3">
                            <label className="text-title-menu-food"><i className="fas fa-car mr-2"></i>Parqueadero</label>
                        </div>
                        <div className="col-9">
                            <Alert color="info">
                                <span className="h5 text-white"><strong>ATENCIÓN</strong>, los siguientes datos son de uso exclusivo para ser agregados cuando se creen planes turisticos</span>
                            </Alert>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2">
                            <Menu model={this.items} />
                        </div>
                        <div className="col-10">
                            <DataTable value={this.state.serviceAddCar} paginator={true} rows={5} sortMode="multiple" responsive={true} selectionMode="single" selection={this.state.selectedCar} onSelectionChange={e => this.setState({ selectedCar: e.value })} >
                                <Column field="codigo" header="Codigo" sortable={true} style={{ width: '10%' }} filterPlaceholder="Buscar"></Column>
                                <Column field="nombre" header="Vehiculo" sortable={true} filter={true} filterPlaceholder="Buscar"></Column>
                                <Column field="modelo" header="Modelo" sortable={true} filter={true}></Column>
                                <Column field="añoModelo" header="Año" sortable={true} style={{ width: '10%' }} filterMatchMode="contains" filterPlaceholder="Precio"></Column>
                                <Column field="capacidad" header="Capacidad" sortable={true} style={{ width: '10%' }} filterMatchMode="contains" filterPlaceholder="Precio"></Column>
                                <Column field="precioPersona" header="Precio" sortable={true} style={{ width: '10%' }} filterMatchMode="contains" filterPlaceholder="Precio"></Column>
                                <Column field="descripcion" header="Descripcion" sortable={true} filterMatchMode="contains" filterPlaceholder="Precio"></Column>
                            </DataTable>
                        </div>
                    </div>
                    <Dialog header="Registrar vehiculo" visible={this.state.sidebar} style={{ width: '50%' }} modal={true} onHide={() => this.setState({ sidebar: false })} footer={this.footer}>
                    <form id="transporte-form">
                            <div className="container-fluid mt-3 mb-3">
                                <div className="row mt-3">
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Codigo</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.car.codigo} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.codigo = val;
                                                return { car };
                                            })
                                        }}  />
                                    </div>
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Nombre del vehiculo</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.car.nombre} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.nombre = val;
                                                return { car };
                                            })
                                        }}  />
                                       
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Modelo</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.car.modelo} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.modelo = val;
                                                return { car };
                                            })
                                        }}  />

                                    </div>
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Año Modelo</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.car.añoModelo} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.añoModelo = val;
                                                return { car };
                                            })
                                        }}  />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Capacidad del vehiculo</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.car.capacidad} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.capacidad = val;
                                                return { car };
                                            })
                                        }}  />
                                    </div>
                                    <div className="col-6">
                                        <label className="text-dark font-weight-bold">Precio por persona</label>
                                        <InputText style={{ width: '100%' }} id="codigo" value={this.state.car.precioPersona} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.precioPersona = val;
                                                return { car };
                                            })
                                        }}  />
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-12">
                                        <label className="text-dark font-weight-bold">Breve descripcion del vehiculo</label>
                                        <InputTextarea style={{ width: '100%' }} id="codigo" rows={5} cols={30} autoResize={true} value={this.state.car.descripcion} onChange={(e) => {
                                            let val = e.target.value;
                                            this.setState(prevState => {
                                                let car = Object.assign({}, prevState.car);
                                                car.descripcion = val;
                                                return { car };
                                            })
                                        }}  />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Dialog>
                    <Growl ref={(el) => this.growl = el} />
                </Dialog>
                <button className="btn-menu-food" onClick={(e) => this.setState({ visible: true })}  >
                    <label className="label-btn-menu-food">Ingresar</label>
                </button>
            </div>
        );
    }
    showEditDialog() {
        this.setState({
            sidebar: true,
            car: {
                codigo: this.state.selectedCar.codigo,
                nombre: this.state.selectedCar.nombre,
                modelo: this.state.selectedCar.modelo,
                añoModelo: this.state.selectedCar.añoModelo,
                capacidad: this.state.selectedCar.capacidad,
                precioPersona: this.state.selectedCar.precioPersona,
                descripcion: this.state.selectedCar.descripcion
            }
        })
    }

}
