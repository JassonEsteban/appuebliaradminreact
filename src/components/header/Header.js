/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";

class Header extends React.Component {
    render() {
        return (
            <div className="header fondo-header-card pb-4 pt-5 pt-md-7">
                <Container fluid>
                    <div className="header-body">
                        {/* Card stats */}
                        <Row>
                            <Col lg="6" xl="3">
                                <Card className="card-stats mb-4 mb-xl-0">
                                    <CardBody>
                                        <Row>
                                            <div className="col">
                                                <CardTitle
                                                    tag="h5"
                                                    className="text-uppercase text-muted mb-0">
                                                    Planes
                                                </CardTitle>
                                                <span className="h4 font-weight-bold mb-0">
                                                    250 planes
                                                </span>
                                            </div>
                                            <Col className="col-auto">
                                                <div className="icon icon-shape bg-green-one text-white rounded-circle shadow">
                                                <span className="material-icons">card_travel</span>
                                                </div>
                                            </Col>
                                        </Row>
                                        <p className="mt-3 mb-0 text-muted text-sm">
                                            <a className="text-black informe-descarga" href="">ver detalles</a>
                                            <span className="text-nowrap">Actualizado hace 3 min</span>
                                        </p>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col lg="6" xl="3">
                                <Card className="card-stats mb-4 mb-xl-0">
                                    <CardBody>
                                        <Row>
                                            <div className="col">
                                                <CardTitle
                                                    tag="h5"
                                                    className="text-uppercase text-muted mb-0">
                                                    Proveedores
                                                </CardTitle>
                                                <span className="h4 font-weight-bold mb-0">
                                                    562 actuales
                                                </span>
                                            </div>
                                            <Col className="col-auto">
                                                <div className="icon icon-shape bg-green-two text-white rounded-circle shadow">
                                                <span className="material-icons">hotel</span>
                                                </div>
                                            </Col>
                                        </Row>
                                        <p className="mt-3 mb-0 text-muted text-sm">
                                            <a className="text-black informe-descarga" href="">ver detalles</a>
                                            <span className="text-nowrap">Actualizado hace 3 min</span>
                                        </p>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col lg="6" xl="3">
                                <Card className="card-stats mb-4 mb-xl-0">
                                    <CardBody>
                                        <Row>
                                            <div className="col">
                                                <CardTitle
                                                    tag="h5"
                                                    className="text-uppercase text-muted mb-0">
                                                    Usuarios
                                                </CardTitle>
                                                <span className="h4 font-weight-bold mb-0">
                                                 269 registrados
                                                </span>
                                            </div>
                                            <Col className="col-auto">
                                                <div className="icon icon-shape bg-green-three text-white rounded-circle shadow">
                                                <span className="material-icons">accessibility</span>
                                                </div>
                                            </Col>
                                        </Row>
                                        <p className="mt-3 mb-0 text-muted text-sm">
                                            <a className="text-black informe-descarga" href="">ver detalles</a>
                                            <span className="text-nowrap">Actualizado hace 6 min</span>
                                        </p>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col lg="6" xl="3">
                                <Card className="card-stats mb-4 mb-xl-0">
                                    <CardBody>
                                        <Row>
                                            <div className="col">
                                                <CardTitle
                                                    tag="h5"
                                                    className="text-uppercase text-muted mb-0">
                                                    Ventas 
                                                </CardTitle>
                                                <span className="h4 font-weight-bold mb-0">
                                                    952 planes vendidos
                                                </span>
                                            </div>
                                            <Col className="col-auto">
                                                <div className="icon icon-shape bg-green-four text-white rounded-circle shadow">
                                                <span className="material-icons">attach_money</span>
                                                </div>
                                            </Col>
                                        </Row>
                                        <p className="mt-3 mb-0 text-muted text-sm">
                                            <a className="text-black informe-descarga" href="">ver detalles</a>
                                            <span className="text-nowrap">Actualizado hace 9 min</span>
                                        </p>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </div>
        );
    }
}

export default Header;