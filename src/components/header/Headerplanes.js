import React from "react";
import {Container, Row, Col} from "reactstrap";

class Headerplanes extends React.Component{
    render(){
        return(
            <>
            < div className="header pb-5 pt-2 pt-lg-6 d-flex align-items-center" style={{
                    minHeight: "100px",
                    backgroundImage: "url(" + require("assets/img/42298581.jpg") + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "center top"
                }}>
                    <span className="mask bg-gradient-banner  opacity-6" />
                    <Container fluid>
                        <Row>
                            <Col lg="6" xl="6">
                                <h5 className="display-1 text-white text-title-user">appuebliar | planes </h5>
                            </Col>
                        </Row>
                    </Container>
            </div>
            </>
        );
    }
}

export default Headerplanes;