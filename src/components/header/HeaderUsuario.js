import React from "react";
import { Container, Row, Col} from "reactstrap";

class HeaderUsuario extends React.Component {
    render() {
        return (
            <>
                < div className="header pb-5 pt-2 pt-lg-6 d-flex align-items-center" style={{
                    minHeight: "200px",
                    backgroundImage: "url(" + require("assets/img/img13.jpg") + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "center top"
                }}>
                    <span className="mask bg-gradient-banner  opacity-4" />

                    <Container fluid>
                        <Row>
                            <Col lg="6" xl="6">
                                <h5 className="text-title-user">appuebliar | proveedores</h5>
                            </Col>
                        </Row>
                       
                    </Container>


            </div>

            </>
        );
    }
}

export default HeaderUsuario;