import React from "react";
import { Link } from "react-router-dom";

import { DropdownItem, UncontrolledDropdown, DropdownToggle, Navbar, Nav, Container, Media, DropdownMenu} from "reactstrap"

class AdminNavbar extends React.Component{
    render(){
        return(
            <Navbar className="navbar-dark navbar-top" expand="md" id="navbar-main">
                <Container fluid>
                    <Link className="label-indication-page" to="/auth/login.js">
                    </Link>
                    <Nav className="align-items-center d-none d-md-flex" navbar>
                        <UncontrolledDropdown nav>
                            <DropdownToggle className="pr-0" nav>
                                <Media className="align-items-center">
                                    <span className="avatar  rounded-circle shadow">
                                    <span className="material-icons">person</span>
                                    </span>
                                    <Media className="ml-2 d-none d-lg-block">
                                        <span className="mb-0 text-sm font-weight-bold">Hola administrador </span>
                                    </Media>
                                </Media>
                            </DropdownToggle>
                            <DropdownMenu className="dropdown-menu-arrow" right>
                                <DropdownItem className="noti-title" header tag="div">
                                    <h6 className="text-overflow m-0">Bienvenido</h6>
                                </DropdownItem>
                                <DropdownItem to="/admin/user-profile">
                                <span className="material-icons icons-perfil">face</span>
                                    <span>Mi perfil</span>
                                </DropdownItem>
                                <DropdownItem to="/admin/proveedores">
                                <span className="material-icons icons-perfil">settings</span>
                                    <span>Configuración</span>
                                </DropdownItem>
                                <DropdownItem to="/admin/user-profile">
                                <span className="material-icons icons-perfil">insert_chart</span>
                                    <span>Estadisticas</span>
                                </DropdownItem>
                                <DropdownItem to="/auth/login" tag={Link}>
                                <span className="material-icons icons-perfil">exit_to_app</span>
                                    <span className="logout">Cerrar sesion</span>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Container>
            </Navbar>
        );
    }

    cerrarsesion(){
        window.location.assign = "/auth/login.js";
    }
}

export default AdminNavbar;