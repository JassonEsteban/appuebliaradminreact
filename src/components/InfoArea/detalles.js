import React from 'react';
import { Card, CardTitle, Row, Col, CardFooter, CardBody, FormGroup, Label, Input, CardHeader } from 'reactstrap';
import HeaderUserplan from "components/header/Headerplanes.js";

class detalle extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const { values, handleChange } = this.props;
        return (
            <div className="">
                <HeaderUserplan />
                <div className="mr-8 ml-8 mt-2 mb-1 border-black">
                    <Card>
                        <CardHeader>
                            <div className="mt-1">
                                <CardTitle className="text-center text-black font-weight-bold h1 text-darker"><i class="fas fa-bus mr-2"></i> Datos de salida</CardTitle>
                            </div>
                        </CardHeader>
                        <CardBody>

                            <Row form>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label for="exampleEmail" className="form-text-plan" >Lugar de salida</Label>
                                        <Input type="text" name="namePlan" id="examplePlan" />
                                    </FormGroup>
                                </Col>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label for="exampleEmail" className="form-text-plan">Fecha de salida</Label>
                                        <Input type="date" name="namePlan" id="examplePlan" />
                                    </FormGroup>
                                </Col>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label for="exampleEmail" className="form-text-plan">Fecha de regreso</Label>
                                        <Input type="date" name="namePlan" id="examplePlan" />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <div>
                                <Row form>
                                    <Col md={2}>
                                        <FormGroup>
                                            <Label for="exampleEmail" className="form-text-plan">Año</Label>
                                            <Input type="text" name="origen" id="exampleOrigen" placeholder="Ingrese el año actual" />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <Label for="examplePassword" className="form-text-plan">Mes</Label>
                                            <Input type="text" name="destino" id="exampleDestino" placeholder="Ingrese el mes actual o el mes donde esta el plan" />
                                        </FormGroup>
                                    </Col>
                                    <Col md={8}>
                                        <FormGroup>
                                            <Label for="examplePassword" className="form-text-plan">Descripcion de la salida</Label>
                                            <Input type="textarea" name="destino" id="exampleDestino" placeholder="Ingrese una descripcion breve sobre la salida" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </div>

                        </CardBody>
                        <CardFooter>
                       
                            <button className="button-create-plan-one " onClick={this.continue}>
                                <span className="label-bottom">Guardar y continuar</span>
                                <i className="fas fa-arrow-right ml-2 label-bottom"></i>
                            </button>
                        </CardFooter>
                    </Card>
                </div>



            </div>
        );
    }
}

export default detalle;