import React from 'react';
import { Card,  CardHeader, CardFooter, CardBody, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import HeaderUserplan from "components/header/Headerplanes.js";

class planes extends React.Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    render() {
        const { values, handleChange } = this.props;
        return (

            <div className="">
                <HeaderUserplan />
                <React.Fragment>
                    <div className="mr-8 ml-8 mt-2 mb-1 border-black">
                        <Card>
                            <CardHeader>
                                <Row>
                                    <Col md={9} className="text-center text-black font-weight-bold h1 text-darker">
                                    <i class="fas fa-leaf mr-2"></i>
                                        <Label >Armar un plan</Label>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <label className="title-admin-plan mr-3">Creador</label>
                                            <input type="text" className="input" onChange={handleChange('codigo')} defaultValue={values.codigo} id="exampleOrigen" disabled ></input>
                                        </FormGroup>

                                    </Col>
                                   
                                </Row>
                        </CardHeader>
                            <CardBody>
                                <Row form >
                                    <Col md={4}>
                                        <FormGroup>
                                            <Label for="exampleEmail" className="form-text-plan">Codigo</Label>
                                            <Input  type="text" name="codigo" onChange={handleChange('codigo')} defaultValue={values.codigo} id="exampleOrigen" disabled />
                                        </FormGroup>
                                    </Col>
                                    <Col md={8}>
                                        <FormGroup>
                                            <Label for="exampleEmail" className="text-dark"><i class="fas fa-mobile-alt mr-2"></i>Ingrese un nombre del plan (APP)</Label>
                                            <Input className="input-form-step-one" type="text" name="namePlan" id="examplePlan" onChange={handleChange('nombrePlan')} defaultValue={values.nombrePlan} className="box-name-plan " placeholder="Ingrese un nombre que indentificara su plan ante los aventureros" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <div>
                                    <Row form>

                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="examplePassword" className="form-text-plan">Seleccione el origen</Label>
                                                <Input type="text" name="destino" onChange={handleChange('origen')} defaultValue={values.origen} id="exampleDestino" placeholder="Ingrese la ciudad de destino" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup>
                                                <Label for="examplePassword" className="form-text-plan"> seleccione el destino</Label>
                                                <Input type="text" name="destino" onChange={handleChange('destino')} defaultValue={values.destino} id="exampleDestino" placeholder="Ingrese la ciudad de destino" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row form>
                                        <Col md={8}>
                                            <FormGroup>
                                                <Label for="exampleEmail" className="form-text-plan">Descripcion del plan</Label>
                                                <Input type="textarea" name="origen" onChange={handleChange('descripcion')} defaultValue={values.descripcion} id="exampleOrigen" placeholder="Ingrese una breve descripcion relacionada con el plan" />
                                            </FormGroup>
                                        </Col>
                                        <Col md={4}>
                                            <FormGroup>
                                                <Label for="exampleSelect" className="form-text-plan">Seleccione el tipo de plan</Label>
                                                <Input type="select" name="select" id="exampleSelect" onChange={handleChange('tipoPlan')} defaultValue={values.tipoPlan}>
                                                    <option>Todo incluido</option>
                                                    <option>Familiar - todo incluido</option>
                                                    <option>Parejas - todo incluido</option>
                                                    <option>Empresarial - todo incluido</option>
                                                    <option>Temporada alta</option>
                                                </Input>
                                            </FormGroup>
                                        </Col>

                                    </Row>
                                </div>


                            </CardBody>
                            <CardFooter >
                                <button className="button-create-plan-one "  onClick={this.continue}>
                                <span className="label-bottom">Guardar y continuar</span>
                                <i className="fas fa-arrow-right ml-2 label-bottom"></i>
                                </button>
                            </CardFooter>
                        </Card>
                    </div>

                </React.Fragment>



            </div>
        );
    }

}

export default planes;
