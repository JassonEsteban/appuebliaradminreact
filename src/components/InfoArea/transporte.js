import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, CardImgOverlay, FormGroup, Label, Input, CardImg, CardBody } from 'reactstrap';
import classnames from 'classnames';

const Example = (props) => {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  }

  return (

    <div className="mt--2">
       <div className="header-two-provider">
      <span>¿Que desea hacer?</span>
    </div>
      <Nav className="mt-5" tabs>
        <NavItem className="pestaña-tab">
          <NavLink className={classnames({ active: activeTab === '1' })} onClick={() => { toggle('1'); }}>
            <span className="text-black text-tab"><span class="material-icons icon-tab">directions_bus</span>Transporte</span></NavLink>
        </NavItem>
        <NavItem className="pestaña-tab">
          <NavLink className={classnames({ active: activeTab === '2' })} onClick={() => { toggle('2'); }}>
            <span className="text-black text-tab"><span class="material-icons icon-tab">fastfood</span>Alimentación</span></NavLink>
        </NavItem>
        <NavItem className="pestaña-tab">
          <NavLink className={classnames({ active: activeTab === '3' })} onClick={() => { toggle('3'); }}>
            <span className="text-black text-tab"><span class="material-icons icon-tab">directions_bike</span>Actividades</span></NavLink>
        </NavItem>
        <NavItem className="pestaña-tab">
          <NavLink className={classnames({ active: activeTab === '4' })} onClick={() => { toggle('4'); }}>
            <span className="text-black text-tab"><span class="material-icons icon-tab">hotel</span>Alojamiento</span></NavLink>
        </NavItem>
      </Nav>

      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="6">
              <div className="mt-1">
                <Card>
                  <CardBody>
                    <CardTitle className="text-card-vehicle mt-3 mb-4"><span class="material-icons color-icon-provider">add_box</span>Registrar un proveedor de transporte</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">NIT</Label>
                          <Input type="text" name="nit" id="exampleNit" placeholder="Ingrese el NIT del proveedor" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre del proveedor</Label>
                          <Input type="text" name="nameProvider" id="exampleProvider" placeholder="Ingrese el nombre de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Contacto</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Numero de telefono" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Dirección</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Dirección de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="12">
                        <FormGroup>
                          <Label for="exampleText" className="text-label-provider">Descripción</Label>
                          <Input type="textarea" name="text" id="exampleText" placeholder="Descripcion breve de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Ubicación</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una ciudad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="label-form"><span class="material-icons icon-pueblo">nature_people</span>Puntos pueblo</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Puntos a ganar" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button className="button-submit-provider">Registrar <span class="material-icons icon-submit">check</span></Button>
                  </CardBody>
                </Card>
              </div>

            </Col>
            <Col sm="6">
              <div className="mt-1">
                <Card className="card-provider-one">
                  <CardBody>
                    <CardTitle className="text-card-vehicle">Vincular un vehiculo</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Codigo</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Codigo del vehiculo" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre del vehiculo</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese el nombre del vehiculo" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Modelo</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese el modelo" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Año modelo</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Año del vehiculo" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Capacidad</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="capacidad de personas" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Precio transporte x persona</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Precio de trnasporte por persona" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="12">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Descripcion del vehiculo</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una breve dewcripcion del vehiculo" />
                        </FormGroup>
                      </Col>

                    </Row>
                    <Button className="button-submit-provider">Vincular<span class="material-icons icon-submit">directions_car</span></Button>
                  </CardBody>
                </Card>
              </div>
            </Col>
          </Row>



        </TabPane>
        <TabPane tabId="2">
          <Row>
            <Col sm="6">
              <div className="mt-1">
                <Card>
                  <CardBody>
                    <CardTitle className="text-card-vehicle mt-3 mb-4">Registrar un restaurante</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">NIT</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese el NIT del proveedor" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre del proveedor</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese el nombre de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Contacto</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Numero de telefono" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Dirección</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Dirección de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="12">
                        <FormGroup>
                          <Label for="exampleText" className="text-label-provider">Descripción</Label>
                          <Input type="textarea" name="text" id="exampleText" placeholder="Descripcion breve de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Ubicación</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una ciudad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Puntos pueblo</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Puntos a ganar" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Capacidad de atención</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una ciudad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">URL web</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Puntos a ganar" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button>Registrar</Button>
                  </CardBody>
                </Card>
              </div>

            </Col>
            <Col sm="6">
              <div className="mt-1">
                <Card>
                  <CardBody>
                    <CardTitle className="text-card-vehicle mt-3 mb-4">Agregar un plato</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Codigo</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Codigo del vehiculo" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre del plato</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese el nombre del vehiculo" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Descripcion</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese el modelo" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Precio unitario</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Año del vehiculo" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button>Agregar al menu</Button>
                  </CardBody>
                </Card>
              </div>
            </Col>
          </Row>
        </TabPane>

        <TabPane tabId="3">
          <Row>
            <Col sm="6">
              <div className="mt-1">
                <Card>
                  <CardBody>
                    <CardTitle className="text-card-vehicle mt-3 mb-4">Agregar una actividad</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Codigo</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese el codigo de la actividad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre de la actividad</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese un nombre" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Fecha de inicio</Label>
                          <Input type="date" name="email" id="exampleEmail" placeholder="" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Fecha de terminación</Label>
                          <Input type="date" name="password" id="examplePassword" placeholder="" />
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Ubicacion</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una ciudad" />
                        </FormGroup>
                      </Col>

                    </Row>

                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Operador</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese el operador de la actividad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Precio por persona</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese un precio" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Hora de inicio</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una hora" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Descripcion</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese una breve descripcion" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Capacidad de personas</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="capacidad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Lugar de la actividad</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese una direccion o un sitio" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Contacto</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese un telefono de contacto" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Puntos pueblo</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Puntos a ganar" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button>Registrar</Button>
                  </CardBody>
                </Card>
              </div>
            </Col>

            <Col sm="6">
              <div className="mt-6">
                <Card inverse>
                  <CardImg className="mascara dark" width="100%" src={require("assets/img/img17.jpg")} alt="Card image cap" />
                  <CardImgOverlay>
                    <CardTitle className="h1 text-white mt-5 fuente-card">Boleteria a eventos y conciertos</CardTitle>
                    <CardText className="h2 text-white">Tiquetera appuebliar </CardText>
                    <CardText>
                      <h2 className="text-muted text-white font-weight-bold mt-9">PROXIMAMENTE</h2>
                    </CardText>
                  </CardImgOverlay>
                </Card>
              </div>

            </Col>

          </Row>
        </TabPane>

        <TabPane tabId="4">
          <Row>
            <Col sm="6">
              <div className="mt-1">
                <Card>
                  <CardBody>
                    <CardTitle className="text-card-vehicle mt-3 mb-4">Registrar un alojamiento ( Hotel - Hostal)</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">NIT</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese el NIT del proveedor" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre del proveedor</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese el nombre de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Contacto</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Numero de telefono" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Dirección</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Dirección de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Fecha de ingreso</Label>
                          <Input type="date" name="email" id="exampleEmail" placeholder="Numero de telefono" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Fecha de salida</Label>
                          <Input type="date" name="password" id="examplePassword" placeholder="Dirección de la empresa" />
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Ubicación</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese una ciudad" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Puntos pueblo</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Puntos a ganar" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">URL web</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Puntos a ganar" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button>Registrar</Button>
                  </CardBody>
                </Card>
              </div>

            </Col>
            <Col sm="6">
              <div className="mt-1">
                <Card>
                  <CardBody>
                    <CardTitle className="text-card-vehicle mt-3 mb-4">Agregar una habitacion</CardTitle>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Codigo</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Codigo de la habitacion" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Nombre de la habitación</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese el nombre de la habitacion" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Descripcion</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Breve descripcion" />
                        </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="examplePassword" className="text-label-provider">Precio x noche</Label>
                          <Input type="text" name="password" id="examplePassword" placeholder="Ingrese el precio" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label for="exampleEmail" className="text-label-provider">Capacidad de la habitacion</Label>
                          <Input type="text" name="email" id="exampleEmail" placeholder="Ingrese la capacidad de la habitacion" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button>Agregar habitacion</Button>
                  </CardBody>
                </Card>
              </div>
            </Col>
          </Row>
        </TabPane>



      </TabContent>
    </div>
  );
}

export default Example;
