import React from 'react';
import { Card, Button, CardTitle, Row, Col, CardFooter, CardBody, FormGroup, Label, Input, CardHeader } from 'reactstrap';
import HeaderUserplan from "components/header/Headerplanes.js";
import { PickList } from 'primereact/picklist';

class proveedoresAD extends React.Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }

    render(){
         return (
        <div className="">
            <HeaderUserplan />
            <div className="mr-8 ml-8 mt-6 mb-1 border-black">
                <Card>
                    <CardHeader>
                        <div className="mt-1">
                            <CardTitle className="text-center text-black font-weight-bold h1 text-darker"><i class="fas fa-utensils mr-2"></i> Proveedores del plan</CardTitle>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleSelect" className="form-text-plan">Seleccione transporte</Label>
                                    <Input type="select" name="select" id="exampleSelect">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleSelect" className="form-text-plan">Seleccione alimentación</Label>
                                    <Input type="select" name="select" id="exampleSelect">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                        </Row>
                        <div>
                            <Row form>
                                <Col md={6}>
                                    <FormGroup>
                                        <Label for="exampleSelect" className="form-text-plan">Selecione alojamiento</Label>
                                        <Input type="select" name="select" id="exampleSelect">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup>
                                        <Label for="exampleSelect" className="form-text-plan">Seleccione actividades</Label>
                                        <Input type="select" name="select" id="exampleSelect">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </Input>
                                    </FormGroup>
                                </Col>

                            </Row>
                        </div>
                    </CardBody>
                    <CardFooter>
                        <button className="button-create-plan-one " onClick={this.continue}>
                            <span className="label-bottom">Confirmar</span>
                            <i className="fas fa-arrow-right ml-2 label-bottom"></i>
                        </button>
                    </CardFooter>
                </Card>


            </div>



        </div>
    );
    }

}

export default proveedoresAD;